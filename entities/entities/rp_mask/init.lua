AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')



function ENT:Initialize()
	self.Entity:SetModel("models/vinrax/props/scp035/035_mask.mdl")
	
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:SetUseType( SIMPLE_USE )

	self.Entity.Owner = nil 
end

function ENT:Use(ply, caller)
	--if (CLIENT) then return end
	if (!IsValid(ply)) then return end
	
	local maskPly = self.Entity.Owner
	local classTbl = baseclass.Get( player_manager.GetPlayerClass(ply) )
	
	maskPly:Spectate(OBS_MODE_NONE)
	maskPly:UnSpectate()
	maskPly:Spawn()
	maskPly:SetPos( ply:GetPos() )
	maskPly:SetModel(ply:GetModel())	
	
	
	maskPly:SetWalkSpeed(classTbl.WalkSpeed)
	maskPly:SetRunSpeed(classTbl.RunSpeed)

	ply:UnSpectate()
	ply:Spectate( OBS_MODE_CHASE )
	ply:SpectateEntity( maskPly )
	local weps = ply:GetWeapons()
	ply:StripWeapons()

	for k,v in pairs(weps) do
		maskPly:Give(v:GetClass())
	end
	
	self:Remove()
	SCP_MASK_ENT = nil

end

function ENT:OnTakeDamage(dmg)

end