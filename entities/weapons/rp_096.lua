
AddCSLuaFile()

SWEP.Author			= "47goon"
SWEP.Purpose		= ""

SWEP.Spawnable			= true
SWEP.UseHands			= false

SWEP.ViewModel				= "models/weapons/v_arms_scp096.mdl"
SWEP.WorldModel				= ""

SWEP.ViewModelFOV		= 56

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 10
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.PrintName			= "SCP 096"
SWEP.Slot				= 1
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false
SWEP.seenPlys = {}



function SWEP:Initialize()
	self:SetWeaponHoldType( "knife" )
	util.PrecacheSound("weapons/scp96/attack1.wav")
	util.PrecacheSound("weapons/scp96/attack2.wav")
	util.PrecacheSound("weapons/scp96/attack3.wav")
	util.PrecacheSound("weapons/scp96/attack4.wav")
	util.PrecacheSound("weapons/scp96/096_1.wav")
	util.PrecacheSound("weapons/scp96/096_2.wav")
	util.PrecacheSound("weapons/scp96/096_3.wav")
	util.PrecacheSound("weapons/scp96/096_idle1.wav")
	util.PrecacheSound("weapons/scp96/096_idle2.wav")
	util.PrecacheSound("weapons/scp96/096_idle3.wav")
	self:CallOnClient( "PrimaryAttack" )
	self.Owner.seenPlys = {}

end


function SWEP:PrimaryAttack()
	
	self.Owner:SetAnimation( PLAYER_ATTACK1 );
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	self.Owner:ViewPunch( Angle( -10, 0, 0 ) )
	self.Weapon:EmitSound( "weapons/scp96/attack"..math.random(1,4)..".wav" )

	local trace = self.Owner:GetEyeTrace();
	if (trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 and trace.Entity and trace.Entity:IsPlayer() and !isScp( player_manager.GetPlayerClass(trace.Entity) ) ) then
		if (SERVER) then
			trace.Entity:Kill()
		end
		
	end
	
	self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
end

function SWEP:Deploy()
	if self.Owner:IsValid() then
	self.Weapon:EmitSound( "weapons/scp96/096_idle"..math.random(1,3)..".wav" )
	
	self:SendWeaponAnim( ACT_VM_DRAW )
	timer.Simple(0.9, function(wep) self:SendWeaponAnim(ACT_VM_IDLE) end)
	end
	return true;
end

function SWEP:Think()
	if (CLIENT) then
		for k,v in pairs(self.seenPlys) do
			if ((v:IsPlayer() and !v:Alive()) or v == nil) then
				table.RemoveByValue(self.seenPlys, v)
			end
		end
		for k,v in pairs(ents.FindInSphere(self.Owner:GetPos(), 500)) do
			if (v:IsPlayer() and v:Alive() and canSee2(v, self.Owner)) then
				if ( table.HasValue(self.seenPlys, v)) then return end
				table.insert(self.seenPlys, v)
			end
		end	
	end
end

function SWEP:SecondaryAttack()
	if(CLIENT) then return end
	
	--EZ code but credits to vinrax's weapon, copied cause lazy :/
	local ent = self.Owner:GetEyeTrace().Entity
	if ent != NULL && ent:IsValid() && (ent:GetClass() == "func_door" || ent:GetClass() == "func_door_rotating" || ent:GetClass() == "prop_door_rotating") && ent:GetPos():Distance(self.Owner:GetPos()) < 90 then
		for k,v in pairs(ents.FindByName(ent:GetName())) do
			if !table.HasValue({"heavydoor_57","heavydoor_55","heavydoor_56","elevator1_doors","682_doors"},v:GetName()) then
				v:Fire("Open")
				
				self.Owner:EmitSound("doors/heavy_metal_stop1.wav")
			end
		end
	end
	self:SetNextSecondaryFire(CurTime()+1)
end

hook.Add( "PreDrawHalos", "AddHalos2", function()
	if(LocalPlayer():GetActiveWeapon().seenPlys) then
		halo.Add( LocalPlayer():GetActiveWeapon().seenPlys, Color( math.sinwave( 2, 255, true), 0, 0 ), 5, 5, 2, true, true )
	end
	
end )