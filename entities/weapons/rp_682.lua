
AddCSLuaFile()

SWEP.Author			= "47goon"
SWEP.Purpose		= ""

SWEP.Spawnable			= true
SWEP.UseHands			= true

SWEP.ViewModel				= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel				= ""

SWEP.ViewModelFOV		= 56

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 10
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.PrintName			= "SCP 682"
SWEP.Slot				= 1
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false

SWEP.doors = {}

SWEP.RageCounter = 0
SWEP.RagePhase = 1
SWEP.biteDamage = 20
SWEP.timeStamp = 0


function SWEP:Initialize()
	self:SetWeaponHoldType( "knife" )
	util.PrecacheSound("weapons/scp682/roar.wav")
	util.PrecacheSound("weapons/scp682/bite.wav")
	util.PrecacheSound("weapons/scp682/growl.wav")
	self:CallOnClient( "PrimaryAttack" )

	self.RagePhase = 1
	self.biteDamage = self.RagePhase * 20
	
end


function SWEP:BreakDoors()
	if (CLIENT) then return end
	
	
	for k,v in pairs(ents.FindInSphere(self.Owner:GetPos(), 75) ) do
		if(v:GetClass() == "func_door" or v:GetClass() == "func_door_rotating" or v:GetClass() == "prop_door_rotating"  ) then
			--if (v:GetMaterials()[1] == "desertpack/door09") then return end
			if (table.HasValue({"heavydoor_57","heavydoor_55","heavydoor_56","elevator1_doors","682_doors"},v:GetName())) then return end
			v:EmitSound( "physics/wood/wood_panel_break1.wav" , 150, 100, 1, CHAN_AUTO )
			util.ScreenShake( self.Owner:GetPos(), 2, 2, 1, 2500 )
			v:Fire('SetSpeed', '500', 0);
			v:Fire('unlock', '', 0);
			v:Fire('open', '', 0);
			table.insert(self.doors, v)
		end
	end

	timer.Simple(1, function()
		for k,v in pairs(self.doors) do
			if (v:GetMaterials()[1] == "ttt_foundation/door01") then
				v:Fire('SetSpeed', '25', 0);
			else
				v:Fire('SetSpeed', '100', 0);
			end
			
			self.doors = {}
		end
	end)
end

function SWEP:Think()
	if (CLIENT) then return end
	if(self.RagePhase >= 10) then return end

	if(self.RagePhase >= 10 and self.RageCounter >= 2000) then
		--DO LIMP CODE HERE
		self.Owner:Kill()
	end
	self.biteDamage = self.RagePhase * 20
	if (self.RageCounter and self.RageCounter >= 500) then
		self.RageCounter = 0
		self.RagePhase = self.RagePhase + 1
		self.Owner:SetNetworkedInt("ragePhase", self.RagePhase )
		--print("RESET")
	end
	if(self.timeStamp <= CurTime() and self.RagePhase > 1) then
		self.RageCounter = 0
		self.RagePhase = 1
		self.Owner:SetNetworkedInt("ragePhase", self.RagePhase )
		self.timeStamp = CurTime() + 5*60
	end
end


function SWEP:PrimaryAttack()
	if (CLIENT) then return end
	self:BreakDoors()
	local plys = ents.FindInSphere(self.Owner:GetPos(), 150)
	local dmginfo = DamageInfo()

	dmginfo:SetInflictor( self )
	local attacker = self.Owner
	if ( !IsValid( attacker ) ) then attacker = self end
	dmginfo:SetAttacker( attacker )
	
	
	dmginfo:SetDamage( self.biteDamage )
	for k,v in pairs(plys) do
		if (v:IsPlayer() and v!=self.Owner) then
			if (canSee(self.Owner, v:GetPos())) then
				v:TakeDamageInfo( dmginfo )
				self.Owner:EmitSound( "weapons/scp682/bite.wav" , 150, 100, 1, CHAN_AUTO )
			end
		end
	end

	self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
end

function SWEP:SecondaryAttack()
	self.Owner:ViewPunch( Angle( -10, 0, 0 ) )
	self.Owner:EmitSound( "weapons/scp682/roar.wav" , 150, 100, 1, CHAN_AUTO )
	if(SERVER) then
		util.ScreenShake( self.Owner:GetPos(), 10, 5, 4, 5000 )
	end
	self.Weapon:SetNextSecondaryFire( CurTime() + 30 )
end



SWEP.rageHeight = 50
SWEP.rageWidth = 200*2
function SWEP:DrawHUD()
	--if (!self.Owner:GetNetworkedBool("inRage")) then return end
	draw.DrawText( "Rage Meter", "scp_caviar_info", ScrW() * 0.5, 4 , Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
	draw.RoundedBox( 10, ScrW()/2 - self.rageWidth/2, 55, self.rageWidth, self.rageHeight, Color(0,0,0, 125) )

	local rageNum = (self.Owner:GetNetworkedInt("ragePhase")*20-5) * 2
	draw.RoundedBox( 10, ScrW()/2 - self.rageWidth/2 + 5, 55 + 5 , rageNum , self.rageHeight - 10, Color(125,0,0, 200) )
	draw.DrawText( "Bite damage: " .. (self.Owner:GetNetworkedInt("ragePhase")*20 or -1), "TargetID", ScrW() * 0.5, ScrH() * 0.25, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
end

hook.Add("EntityTakeDamage", "scp682Rage", function(ply, dmg)
	if (CLIENT) then return end
	if (player_manager.GetPlayerClass(ply) == "player_scp_682" and !ply:GetNetworkedBool("inRage")) then
		ply:SetNetworkedBool("inRage", true)
	end

	if (player_manager.GetPlayerClass(ply) == "player_scp_682" and ply:GetNetworkedBool("inRage")) then
		local dmgNum = dmg:GetDamage()
		local wep = ply:GetActiveWeapon()
		wep.RageCounter = wep.RageCounter + dmgNum
	end
end)


local function Spawn682( ply )
	
	if(player_manager.GetPlayerClass(ply) == "player_scp_682") then
		ply:SetNetworkedInt("ragePhase", 1 )
		ply:SetNetworkedBool("inRage", false)
	end
end
hook.Add( "PlayerSpawn", "some_unique_name", Spawn682 )