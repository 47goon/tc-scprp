
AddCSLuaFile()

SWEP.Author			= "47goon"
SWEP.Purpose		= ""

SWEP.Spawnable			= true

SWEP.ViewModel				= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel				= ""
SWEP.UseHands = true

SWEP.ViewModelFOV		= 56

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 0
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= true
SWEP.AutoSwitchFrom		= true

SWEP.PrintName			= "Build Tool"
SWEP.Slot				= 1
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false

SWEP.propModel = "models/props_c17/oildrum001.mdl"



SWEP.prop = nil
SWEP.prop2 = nil
SWEP.ang = nil
SWEP.propCol = nil
SWEP.boxColor = nil

function SWEP:Initialize()
	self:SetHoldType("normal")
	self.prop = nil
	self.prop2 = nil
	self.ang = Angle(0,0,0)
	self.propCol = Color(46, 204, 113, 175)
	self.boxColor = Color(255,255,255)
	
	if(SERVER) then
		self.propModel = self.Owner:GetNWString("propModel")
	end
end




function SWEP:Deploy()

end

local scp_snap_delay = 0
local shouldDelay = 0
local propSnap = 1
SWEP.rotSpeed = 200
function SWEP:DrawHUD()
	if (SERVER) then return end
	if(!LocalPlayer()) then return end

	if(!self.Owner:GetActiveWeapon():GetClass() == "rp_build") then return end
	if(!self.Owner:GetNWString("propModel")) then return end
	if(self.propModel != self.Owner:GetNWString("propModel")) then self.propModel = self.Owner:GetNWString("propModel") end
	if(!self.prop or !self.prop2) then
		self.prop = ents.CreateClientProp()
		self.prop2 = ents.CreateClientProp()
	end

	local tr = util.TraceLine( {
			start = LocalPlayer():EyePos() ,
			endpos = LocalPlayer():EyePos() + LocalPlayer():EyeAngles():Forward() * 250,
			filter = function( ent ) if ( ent:IsPlayer() ) then return false end if ( ent:GetClass() == "func_door" ) then return true end if ( ent:GetClass() == "prop_physics" ) then return true end  end
	} )

	
	
	self.prop:SetModel(self.propModel or "models/props_c17/oildrum001.mdl")
	self.prop:SetPos( tr.HitPos )
	self.prop:SetPos(Vector(self.prop:GetPos().x, self.prop:GetPos().y, LocalPlayer():GetPos().z))
	self.prop:SetPos(Vector(self.prop:GetPos().x, self.prop:GetPos().y, self.prop:GetPos().z - self.prop:OBBMins().z ))
	self.prop:SetAngles(self.ang)
	self.prop:SetRenderMode( RENDERMODE_TRANSALPHA )
	self.prop:SetColor(self.propCol)
	self.prop:SetMaterial("models/wireframe")
	self.prop:DrawModel()

	self.prop2:SetModel(self.propModel or "models/props_c17/oildrum001.mdl")
	self.prop2:SetPos( tr.HitPos )
	self.prop2:SetPos(Vector(self.prop2:GetPos().x, self.prop2:GetPos().y, LocalPlayer():GetPos().z))
	self.prop2:SetPos(Vector(self.prop2:GetPos().x, self.prop2:GetPos().y, self.prop2:GetPos().z - self.prop2:OBBMins().z))
	self.prop2:SetAngles(self.ang)
	self.prop2:SetRenderMode( RENDERMODE_TRANSALPHA )
	self.prop2:SetColor(self.propCol)
	self.prop2:SetMaterial("models/debug/debugwhite")
	self.prop2:DrawModel()

	if( input.IsKeyDown(KEY_LSHIFT) ) then
		propSnap = 90
		shouldDelay = 0.2
	else
		propSnap = self.rotSpeed*FrameTime()
		shouldDelay = 0
	end
	if( input.IsKeyDown(KEY_Q) and (!scp_snap_delay or scp_snap_delay and scp_snap_delay < CurTime()) ) then
		self.ang = self.ang + Angle(0,propSnap,0)
		scp_snap_delay = CurTime() + shouldDelay
	end

	if(input.IsKeyDown(KEY_E) and (!scp_snap_delay or scp_snap_delay and scp_snap_delay < CurTime()) ) then
		self.ang = self.ang + Angle(0,-propSnap,0)
		scp_snap_delay = CurTime() + shouldDelay
	end

	if(input.IsKeyDown(KEY_R)) then
		self.ang = Angle(0,0,0)
	end

	self.propCol = Color(231, 76, 60)
	tr = {
	start = self.prop:GetPos(),
	endpos = self.prop:GetPos(),
	mins = self.prop:OBBMins(),
	maxs = self.prop:OBBMaxs(),
	filter = function( ent ) 
		if ( ent == self.prop ) then return false end 
		if ( ent:GetClass() == "prop_physics" ) then return true end 
		if ( ent:IsPlayer() ) then return true end 
	end
	}

	local hullTrace = util.TraceHull(tr)

	

	if(hullTrace.Hit) then
		self.boxColor = Color(255,0,0)
	else
		self.boxColor = Color(0,255,0)
	end

	if(hullTrace.Hit) then
		self.propCol = Color(231, 76, 60)
	else
		self.propCol = Color(46, 204, 113, 175)
	end

	tr = util.TraceLine( {
			start = self.prop:GetPos() ,
			endpos = (self.prop:GetPos() - Vector(0,0,self.prop:OBBMaxs().z)) + -self.prop:GetUp() * 5
	} )
	if (!tr.Hit) then
		self.propCol = Color(231, 76, 60)
	end
end

local scp_build_delay = 0
function SWEP:PrimaryAttack()
	if(CLIENT) then
		local ply = self.Owner
		if (!scp_build_delay or scp_build_delay and scp_build_delay < CurTime()) then
			local tr = util.TraceLine( {
			start = ply:EyePos() ,
			endpos = ply:EyePos() + ply:EyeAngles():Forward() * 250,
			filter = function( ent ) if ( ent:IsPlayer() ) then return false end if ( ent:GetClass() == "func_door" ) then return true end end
			} )
			net.Start("scpBUILD")
				net.WriteAngle(self.ang)
				net.WriteString(self.propModel)
			net.SendToServer()
			scp_build_delay = CurTime() + 2
		end

		if(IsValid(self.prop) or IsValid(self.prop2)) then
			self.prop:Remove()
			self.prop2:Remove()
			self.DrawHUD = function() end
		end
	end
	
	if (SERVER) then
		self.Owner:StripWeapon("rp_build")
	end
end

function SWEP:SecondaryAttack()
	if(SERVER) then
		self.Owner:StripWeapon("rp_build")
	end
	if(CLIENT) then
		if(IsValid(self.prop) or IsValid(self.prop2)) then
			self.prop:Remove()
			self.prop2:Remove()
			self.DrawHUD = function() end
		end
		
	end
end

function SWEP:Holster(wep)
	if(SERVER) then
		self.Owner:StripWeapon("rp_build")
	end
	if(CLIENT) then
		if(IsValid(self.prop) or IsValid(self.prop2)) then
			self.prop:Remove()
			self.prop2:Remove()
			self.DrawHUD = function() end
		end
	end
end