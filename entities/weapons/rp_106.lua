
AddCSLuaFile()

SWEP.Author			= "47goon"
SWEP.Purpose		= ""

SWEP.Spawnable			= true
SWEP.UseHands			= true

SWEP.ViewModel				= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel				= ""

SWEP.ViewModelFOV		= 56

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 10
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.PrintName			= "SCP 106 Swp"
SWEP.Slot				= 1
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false

SWEP.transEffect = 5

function SWEP:Initialize()
	self:SetWeaponHoldType( "knife" )
	util.PrecacheSound("weapons/scp96/attack1.wav")
	util.PrecacheSound("weapons/scp96/attack2.wav")
	util.PrecacheSound("weapons/scp96/attack3.wav")
	util.PrecacheSound("weapons/scp96/attack4.wav")
	util.PrecacheSound("weapons/scp96/096_1.wav")
	util.PrecacheSound("weapons/scp96/096_2.wav")
	util.PrecacheSound("weapons/scp96/096_3.wav")
	util.PrecacheSound("weapons/scp96/096_idle1.wav")
	util.PrecacheSound("weapons/scp96/096_idle2.wav")
	util.PrecacheSound("weapons/scp96/096_idle3.wav")
	self:CallOnClient( "PrimaryAttack" )

	self.wallText = "Right click to enter pocket dimension."

	self.Owner:SetNWBool("inPocket", false)
end


function SWEP:PrimaryAttack()
	
	self.Owner:SetAnimation( PLAYER_ATTACK1 );
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	self.Owner:ViewPunch( Angle( -10, 0, 0 ) )
	self.Weapon:EmitSound( "weapons/scp96/attack"..math.random(1,4)..".wav" )

	local trace = self.Owner:GetEyeTrace();
	if (trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 and trace.Entity and trace.Entity:IsPlayer() and !isScp( player_manager.GetPlayerClass(trace.Entity) ) ) then
		if (SERVER) then
			trace.Entity:Kill()
		end
		
	end
	
	self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
end

function SWEP:Deploy()
	self.Owner:SetNWBool("inPocket", false)
	if self.Owner:IsValid() then
	self.Weapon:EmitSound( "weapons/scp96/096_idle"..math.random(1,3)..".wav" )
	
	self:SendWeaponAnim( ACT_VM_DRAW )
	timer.Simple(0.9, function(wep) self:SendWeaponAnim(ACT_VM_IDLE) end)
	end
	return true;
end


function SWEP:DrawHUD()
	local trace = self.Owner:GetEyeTrace();
	if (trace.HitPos:Distance(self.Owner:GetShootPos()) <= 25 and trace.HitWorld and !self.Owner.sinking) then
		if(self.Owner:GetNWBool("inPocket")) then
			self.wallText = "Right click to leave pocket dimension."
		else
			self.wallText = "Right click to enter pocket dimension."
		end

		surface.SetFont( "scp_caviar_info" )
		local w,h = surface.GetTextSize(self.wallText)
		surface.SetTextColor( 0, 0, 0, 255 )
		surface.SetTextPos( ScrW()/2 - w/2 + 2, ScrH() * 0.25)
		surface.DrawText( self.wallText )

		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( ScrW()/2 - w/2, ScrH() * 0.25 )
		surface.DrawText( self.wallText )
	end
end

function SWEP:SecondaryAttack()
	local a = 0
	local trace = self.Owner:GetEyeTrace();
	if (trace.Hit and trace.HitPos  and trace.HitWorld and trace.HitPos:Distance(self.Owner:GetShootPos()) <= 25 ) then
		self.Owner.sinking = true
		if (CLIENT) then
			hook.Add( "HUDPaint", "scp_106_transEffect", function()
				a = Lerp(0.02, a, 256)
				draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( 0, 0, 0, a ) ) -- Draw a box
			end )
		end

		if(SERVER) then
			timer.Create( "106_sink" .. self.Owner:SteamID(), 0.01, self.Owner:OBBMaxs().z + 10, function()
				 self.Owner:SetPos(self.Owner:GetPos() - Vector(0,0,1))
			end )
		end

		timer.Simple( self.transEffect, function() 
			if(SERVER) then
				timer.Remove("106_sink" .. self.Owner:SteamID())
				if(self.Owner:GetNWBool("inPocket")) then
					--outside
					self.Owner:SetPos(Vector(-6906.180176, -4453.828125, -2021.086182))
				else
					--in pocket
					self.Owner:SetPos(Vector(-4361.319824, -6637.419922, -2078.302246))
				end
			end
			hook.Remove("HUDPaint", "scp_106_transEffect") 
			self.Owner.sinking = false
			self.Owner:SetNWBool("inPocket", !self.Owner:GetNWBool("inPocket"))
		end )
		self.Weapon:SetNextSecondaryFire( CurTime() + (self.transEffect + 5) )
	end

	
	
end