local SCP = SCP or {}
local mouseX, mouseY = ScrW() / 2, ScrH() / 2
local logo = Material( "vgui/scpLOGOW.png", "noclamp smooth alphatest" )
--[[---------------------------------------------------------------------------
F4 BACKGROUND
---------------------------------------------------------------------------]]
local PANEL = {}


function PANEL:Init()
	self:SetSize( ScrW(), ScrH() )
	
end

function PANEL:Paint( w, h )
	draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 150 ) )
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4Background", PANEL, "Panel" )

--[[---------------------------------------------------------------------------
F4 SideLOGO
---------------------------------------------------------------------------]]
PANEL = {}

function PANEL:Init()
	self:SetSize( 256, 256 )
	self:Center()
	
	
end

function PANEL:Paint( w, h )
	surface.SetDrawColor( 255, 255, 255, 255 )
	surface.SetMaterial( logo	) -- If you use Material, cache it!
	surface.DrawTexturedRect( 0, 0, self:GetWide(), self:GetTall() )
	
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4scpLogo", PANEL, "Panel" )

--[[---------------------------------------------------------------------------
F4 BtmBorder
---------------------------------------------------------------------------]]
PANEL = {}



function PANEL:Init()
	self:SetSize( ScrW(), ScrH() )
	self:Center()

end

local text = "NULL"
local phrases = {"Welcome to Terminus", "Have a nice stay!", "lol.", "Nice One!", "Go hunting"}
text = phrases[math.Truncate(math.Rand(1,#phrases), 0)]
function PANEL:Paint( w, h )
	
	local grey = 25
	local btmBorder = {
		{ x = 10, y = ScrH() }, --BOTTM LEFT
		{ x = 10, y = ScrH() - 25 }, --TOP LEFT
		{ x = 25, y = ScrH() - 35 },
		{ x = ScrW() - 25, y = ScrH() - 35 }, --TOP RIGHT
		{ x = ScrW() - 10, y = ScrH() - 25 }, --TOP RIGHT
		{ x = ScrW() - 10, y = ScrH() } --BOTTOM RIGHT
	}
	surface.SetDrawColor( grey, grey, grey, 255 )
	draw.NoTexture()
	surface.DrawPoly( btmBorder )
	local multiplier = 100 			-- greater multiplier = faster scroll
	local width = ScrW() * 0.92 			-- the width of hte space which the text will scroll across
	local ypos = ScrH() - 25				-- the y pos on the screen, assuming the text scrolls horizontally
	 	-- the text to draw
	local font = "scp_caviar"	-- the font to draw it in
	local textcol = Color(150,150,150,255)	-- the color to draw the font
	
	if(math.fmod(SysTime() * multiplier,width) <=2) then
		local rand = math.Truncate(math.Rand(1,#phrases), 0)
		text = phrases[rand]
	end
	
	draw.DrawText(text,font,math.fmod(SysTime() * multiplier,width),ypos - 2,textcol,0)
	
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4btmBorder", PANEL, "Panel" )

--[[---------------------------------------------------------------------------
F4 SCP LISTL
---------------------------------------------------------------------------]]
local PANEL = {}

function PANEL:Init()
	self:SetSize( ScrW() * 0.4, ScrH() * 0.8 )
	self:Center()
	local x,y = self:GetPos()
	self:SetPos( ScrW() * 0.01, y)
	
end

function PANEL:Paint( w, h )
	draw.RoundedBox( 10, 0, 0, w, h, Color( 25, 25, 25, 255 ) )
	
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4scpList", PANEL, "Panel" )

--[[---------------------------------------------------------------------------
F4 SCP INNER
---------------------------------------------------------------------------]]
local PANEL = {}

function PANEL:Init()
	self:SetSize( self:GetParent():GetWide() * 0.9, self:GetParent():GetTall() * 0.9)
	self:Center()
	
	
end

function PANEL:Paint( w, h )
	draw.RoundedBox( 0, 0, 0, w, h, Color( 50, 50, 50, 50 ) )
	
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4scpInner", PANEL, "Panel" )



--[[---------------------------------------------------------------------------
F4 SCP LISTR
---------------------------------------------------------------------------]]
local PANEL = {}

function PANEL:Init()
	self:SetSize( ScrW() * 0.4, ScrH() * 0.8 )
	self:Center()
	local x,y = self:GetPos()
	self:SetPos( (ScrW() - self:GetWide()) - ScrW() * 0.01, y)
	self.model = "models/error.mdl"
	self.modelpanel = nil
	
end

function PANEL:Paint( w, h )
	draw.RoundedBox(15, 0, 0, w, h, Color( 25, 25, 25, 255 ) )
	self.modelpanel = vgui.Create( "DModelPanel", self)
	self.modelpanel:SetSize( 300, 300 )
	self.modelpanel:Center()
	local x,y = self.modelpanel:GetPos()
	self.modelpanel:SetPos(x,0)


	
	
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4scpListR", PANEL, "Panel" )

--[[---------------------------------------------------------------------------
F4 SCP LABEL
---------------------------------------------------------------------------]]
local PANEL = {}

function PANEL:Init()
	self:SetSize( 250, 50)
	self:Center()
	local x,y = self:GetPos()
	self:SetPos(x, 10)
	self.grey = 25
	self:SetMouseInputEnabled(true)
	self:SetText("")
	self.text = "NULL"
	self.orgX, self.orgY = self:GetPos()

end


function PANEL:Paint( w, h )
	draw.RoundedBox( 10, 0, 0, w, h, Color( self.grey, self.grey, self.grey, 255 ) )
	draw.DrawText(self.text,"scp_caviar_label",w/2,h/2-17,Color(255,255,255,255),TEXT_ALIGN_CENTER)
	
	if(self:IsHovered()) then
		local x,y = self:GetPos()
		if(y <= self.orgY -4) then
			return
		end
		local mX,mY = gui.MouseX(), gui.MouseY()
		if(mY > y + self:GetTall() - 5) then return end
		self:MoveTo( x, y- 4, 0.1, 0, 0.1)
	else
		local x,y = self:GetPos()
		if(y <= self.orgY - 4) then
			self:MoveTo( self.orgX, self.orgY, 0.1, 0, 0.4)
		end
	end
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4scpLabel", PANEL, "DButton" )

--[[---------------------------------------------------------------------------
F4 SCP LISTITEM
---------------------------------------------------------------------------]]
local PANEL = {}

function PANEL:Init()
	self:SetSize( 10, 10)
	self:SetPos(0,0)
	self:SetMouseInputEnabled(true)

end

function PANEL:Paint( w, h )
	draw.RoundedBox( 0, 0, 0, w, h, Color( 100, 100, 100, 255 ) )
	
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "f4scpListItem", PANEL, "Panel" )



--[[---------------------------------------------------------------------------
INTERFACE FUNCTIONS
---------------------------------------------------------------------------]]
surface.CreateFont("scp_caviar_label", 
{
	size = 30,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})

surface.CreateFont("scp_caviar", 
{
	size = 20,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})

surface.CreateFont("scp_caviar_outline", 
{
	size = 20,
	antialias = "true",
	outline = true,
	font = "Caviar Dreams"

})


surface.CreateFont("scp_caviar_info", 
{
	size = 50,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})

surface.CreateFont("scp_sull", 
{
	size = 100,
	weight = 100,
	font = "Sullivan"

})

local function CheckJobCount(jobStr)
	local cnt = 0
	for k,v in pairs(player.GetAll()) do
		if(player_manager.GetPlayerClass(v) == jobStr) then
			cnt = cnt + 1
		end
	end
	return cnt
end
SCPJOBS = {}
local f4Frame = nil
local f4Logo = nil
local displayTbl = nil
local ply_modelR = "models/error.mdl"
local current_job_title = "NULL"
local current_job_info = "No job info."
function SCP.openF4Menu()
	if(displayTbl == nil) then
		displayTbl = SCPJOBS.SCP_STAFF
	end
	hook.Add( "Think", "scp_escape_menu", function()
			if (input.IsKeyDown( KEY_ESCAPE )) then
				hook.Remove("Think", "scp_escape_menu")
				gui.EnableScreenClicker(false)
				mouseX, mouseY = gui.MousePos()
				SCP.closeF4Menu()
			end
		end )
    if IsValid(f4Frame) and IsValid(f4Logo) then
        f4Frame:Show()
        f4Frame:InvalidateLayout()
    else
        f4Frame = vgui.Create("f4Background")
        f4logo = vgui.Create("f4scpLogo", f4Frame)
        f4bottomBorder = vgui.Create("f4btmBorder", f4Frame)
        
        --LIST SHIT START
        f4scpList = vgui.Create("f4scpList", f4Frame)
        f4scpInnerL = vgui.Create("f4scpInner", f4scpList)

        f4scpListR = vgui.Create("f4scpListR", f4Frame)
        
        
        local Scroll = vgui.Create( "DScrollPanel", f4scpInnerL ) //Create the Scroll panel
		Scroll:SetSize( f4scpInnerL:GetWide(), f4scpInnerL:GetTall() )
		Scroll:SetPos( 0, 0 )

		local sbar = Scroll:GetVBar()
		function sbar:Paint( w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 50, 50, 50, 100 ) )
		end
		function sbar.btnUp:Paint( w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 255, 255, 255,0 ) )
		end
		function sbar.btnDown:Paint( w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 255, 255, 255,0 ) )
		end
		function sbar.btnGrip:Paint( w, h )
			draw.RoundedBox( 25, 0, 0, w, h, Color( 0, 0, 0, 255 ) )
		end

		local List	= vgui.Create( "DIconLayout", Scroll )
		List:SetSize( Scroll:GetWide(), Scroll:GetTall() )
		List:SetPos( 0, 0 )
		List:SetSpaceY( 5 ) //Sets the space in between the panels on the X Axis by 5
		List:SetSpaceX( 5 ) //Sets the space in between the panels on the Y Axis by 5


		
		local x,y = f4scpList:GetPos()
		f4scpSCP_STAFF_TAB = vgui.Create("f4scpLabel", f4Frame)
		f4scpSCP_STAFF_TAB:SetPos(ScrW() * 0.01, y - f4scpSCP_STAFF_TAB:GetTall() -  10)
		f4scpSCP_STAFF_TAB.orgX, f4scpSCP_STAFF_TAB.orgY = f4scpSCP_STAFF_TAB:GetPos()
		f4scpSCP_STAFF_TAB.text = "SCP STAFF"
		f4scpSCP_STAFF_TAB.DoClick = function()
			displayTbl = SCPJOBS.SCP_STAFF
			reloadLIST()
		end

		f4scpSCP_TAB = vgui.Create("f4scpLabel", f4Frame)
		f4scpSCP_TAB:SetPos(ScrW() * 0.01 + f4scpSCP_TAB:GetWide() + 10, y - f4scpSCP_TAB:GetTall() -  10)
		f4scpSCP_TAB.orgX, f4scpSCP_TAB .orgY= f4scpSCP_TAB:GetPos()
		f4scpSCP_TAB.text = "SCP"
		f4scpSCP_TAB.DoClick = function()
			displayTbl = SCPJOBS.SCP
			reloadLIST()
		end

		f4scpNONSCPURITY_TAB = vgui.Create("f4scpLabel", f4Frame)
		f4scpNONSCPURITY_TAB:SetPos(ScrW() * 0.01 + f4scpSCP_TAB:GetWide()  + f4scpNONSCPURITY_TAB:GetWide() + 20, y - f4scpNONSCPURITY_TAB:GetTall() -  10)
		f4scpNONSCPURITY_TAB.orgX, f4scpNONSCPURITY_TAB.orgY = f4scpNONSCPURITY_TAB:GetPos()
		f4scpNONSCPURITY_TAB.text = "NON-SCP"
		f4scpNONSCPURITY_TAB.DoClick = function()
			displayTbl = SCPJOBS.NONSCP
			reloadLIST()
		end

		function reloadLIST()
			List:Clear()
			for i = 1, #displayTbl do //Make a loop to create a bunch of panels inside of the DIconLayout
				local classtbl = baseclass.Get( "player_" .. displayTbl[i].class_filename )
				local ListItem = List:Add( "f4scpListItem" ) //Add DPanel to the DIconLayout
				ListItem:SetSize( Scroll:GetWide(), 100 ) //Set the size of it
				function ListItem:Paint(w,h)
					if(self:IsHovered()) then
						draw.RoundedBox( 0, 0, 0, w, h, Color( 100, 100, 100, 50 ) )
					else
						draw.RoundedBox( 0, 0, 0, w, h, Color( 50, 50, 50, 255 ) )
						if (classtbl.staff_only) then
							--serverguard.ranks:GetRank(serverguard.player:GetRank(player)).color
							draw.RoundedBox( 0, 0, 0, w, h, Color( 255, 200, 50, 10 ) )
						end
					end
					draw.DrawText(displayTbl[i].jobTitle,"scp_caviar",w/2,10,Color(255,255,255,255),TEXT_ALIGN_CENTER)
					draw.DrawText(displayTbl[i].NONSCPurityLevel,"scp_caviar",w/2,h/2-10,Color(255,255,255,255),TEXT_ALIGN_CENTER)
					draw.DrawText(displayTbl[i].class,"scp_caviar",w/2,h-25,Color(255,255,255,255),TEXT_ALIGN_CENTER)
					draw.DrawText( CheckJobCount("player_" .. displayTbl[i].class_filename) .. "/" .. (classtbl.maxPlayers or "Inf") ,"scp_caviar",w-50,h/2-10,Color(255,255,255,255),TEXT_ALIGN_CENTER)
				end
				local icon = vgui.Create( "DModelPanel", ListItem)
				icon:SetSize( 100, ListItem:GetTall() )
				icon:SetModel( displayTbl[i].model )

				local listBtn = vgui.Create("DButton", ListItem)
				listBtn:SetText("")
				listBtn:SetPos(0,0)
				listBtn:SetSize(ListItem:GetWide(), ListItem:GetTall())
				listBtn.Paint = function()
					if(listBtn:IsHovered()) then
						ply_modelR = displayTbl[i].model
						current_job_title = displayTbl[i].jobTitle
						current_job_info = displayTbl[i].info
						draw.RoundedBox( 0, 0, 0, ListItem:GetWide(), ListItem:GetTall(), Color( 100, 100, 100, 25 ) )
						f4scpListR.Paint = function()
							draw.RoundedBox( 15, 0, 0, f4scpListR:GetWide(), f4scpListR:GetTall(), Color( 25, 25, 25, 255 ) )
							if (f4scpListR.modelpanel) then
								local w,h = f4scpListR:GetWide(), f4scpListR:GetTall()
								f4scpListR.modelpanel:SetModel( ply_modelR )
								draw.DrawText((current_job_title or "NULL") .. " info:","scp_caviar_info",w/2,f4scpListR.modelpanel:GetTall() + 10,Color(255,255,255,255),TEXT_ALIGN_CENTER)
								draw.DrawText((current_job_info or "No job info."),"scp_caviar_info",w/2,f4scpListR.modelpanel:GetTall() + 100,Color(255,255,255,255),TEXT_ALIGN_CENTER)
							end
						end
					else
						draw.RoundedBox( 0, 0, 0, ListItem:GetWide(), ListItem:GetTall(), Color( 0, 0, 0, 0 ) )
						
					end
				end
				listBtn.DoClick = function()
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					
					if(classtbl.maxPlayers) then
						if(CheckJobCount("player_" .. displayTbl[i].class_filename) >= classtbl.maxPlayers) then
							notification.AddLegacy( "Job Is Currently Full", NOTIFY_GENERIC, 2 )
							surface.PlaySound( "buttons/button10.wav" )
							mouseX, mouseY = gui.MousePos()
					    	hook.Remove("Think", "scp_f4menu_toggle")
					    	gui.EnableScreenClicker(false)
							SCP.closeF4Menu()
							return
						end
					end
					
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					--FIX THIS IMMEDIATLEY
					net.Start("request_jobChange")
						net.WriteString(displayTbl[i].class_filename)
					net.SendToServer()
					hook.Remove("Think", "scp_escape_menu")
					gui.EnableScreenClicker(false)
					mouseX, mouseY = gui.MousePos()
					SCP.closeF4Menu()
				end
				if(player_manager.GetPlayerClass(LocalPlayer()) == "player_" .. displayTbl[i].class_filename) then ListItem:Remove() end
				if(classtbl.staff_only and serverguard.player:GetImmunity(LocalPlayer()) < 10) then 
					ListItem:Remove() 
				end
			end

		end
		reloadLIST()
		

		
		--LIST SHIT OVER
        
        
        
    end
end
function SCP.closeF4Menu()
    if f4Frame then
        f4Frame:Hide()
    end
end
concommand.Add( "scp_f4menu_toggle", function( ply, cmd, args )
	if not IsValid(f4Frame) or not f4Frame:IsVisible() then
        gui.EnableScreenClicker(true)
        gui.SetMousePos(mouseX, mouseY)
        SCP.openF4Menu()

    else
    	mouseX, mouseY = gui.MousePos()
    	hook.Remove("Think", "scp_f4menu_toggle")
    	gui.EnableScreenClicker(false)
		SCP.closeF4Menu()
    end
end )


concommand.Add( "doCountDown", function( ply, cmd, args )
	local delay = nil
	local counter = 5
	hook.Add( "HUDPaint", "countdown_hud", function()
		if(delay and CurTime() >= delay or !delay) then
			if(counter <= 0) then return end
			counter = counter - 1
			delay = CurTime() + 1
			surface.PlaySound( "HL1/fvox/beep.wav" )
		end
		draw.DrawText( "Changing class in:", "scp_caviar_outline", ScrW()/2, ScrH()/2 - 50, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( counter, "scp_caviar_outline", ScrW() /2, ScrH() /2, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
		draw.DrawText( "Please do not move", "scp_caviar_outline", ScrW() /2 , ScrH() /2 + 50, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
end )
	timer.Create( "plyautoStop" .. ply:SteamID64(), 5, 1, function() hook.Remove("HUDPaint", "countdown_hud") end )
end )

concommand.Add( "stopCountDown", function( ply, cmd, args )
	timer.Remove("plyautoStop" .. ply:SteamID64())
	hook.Remove("HUDPaint", "countdown_hud")
end )

