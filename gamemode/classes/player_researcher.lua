DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/kerry/class_scientist_1.mdl",
	"models/player/kerry/class_scientist_2.mdl",
	"models/player/kerry/class_scientist_3.mdl",
	"models/player/kerry/class_scientist_4.mdl",
	"models/player/kerry/class_scientist_5.mdl",
	"models/player/kerry/class_scientist_6.mdl",
	"models/player/kerry/class_scientist_7.mdl"
}

local spawns = 
{
	Vector(-12893.271484, -5294.622559, -2326.968750),
	Vector(-12776.849609, -5480.639160, -2326.968750),
	Vector(-13002.681641, -5641.269531, -2326.968750),
	Vector(-13082.972656, -5530.845703, -2326.968750),
	Vector(-13094.830078, -5280.269531, -2326.968750),
	Vector(-12701.021484, -5238.077148, -2326.968750),
	Vector(-12561.324219, -5539.134277, -2326.968750)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Researcher"
PLAYER.maxPlayers = 5
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--todo
end

function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "weapon_leash_rope" )
	self.Player:Give( "weapon_fists" )
	self.Player:Give( "rp_hands" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)

end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

function PLAYER:GetHandsModel()

	return { model = "models/weapons/c_arms_cstrike.mdl", skin = 1, body = "0100000" }

end

player_manager.RegisterClass( "player_researcher", PLAYER, "player_default" )