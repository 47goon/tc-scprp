DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/pmc_6/pmc__14.mdl"
}

local spawns = 
{
	Vector(1700, 2010, -1620),
	Vector(1700, 2020, -1620),
	Vector(1700, 2030, -1620),
	Vector(1700, 2040, -1620),
	Vector(1700, 2050, -1620),
	Vector(1700, 2060, -1620),
	Vector(1700, 2070, -1620),
	Vector(1700, 2080, -1620),
	Vector(1700, 2090, -1620),
	Vector(1700, 2100, -1620)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Chaos Insurgent Infilitration"
PLAYER.maxPlayers = 2
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "M9k_svu" )
	self.Player:GiveAmmo( 100, "ar2", true )

	self.Player:Give( "M9k_mp5sd" )
	self.Player:GiveAmmo( 100, "smg", true )

	self.Player:Give( "m9k_knife" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_ci_inf", PLAYER, "player_default" )