DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/kerry/class_d_1.mdl",
	"models/player/kerry/class_d_2.mdl",
	"models/player/kerry/class_d_3.mdl",
	"models/player/kerry/class_d_4.mdl",
	"models/player/kerry/class_d_5.mdl",
	"models/player/kerry/class_d_6.mdl",
	"models/player/kerry/class_d_7.mdl"
}

local spawns = 
{
	Vector(-13851.868164, -6063.469238, -2154),
	Vector(-13731.957031, -6058.822266, -2154),
	Vector(-13599.953125, -6047.165527, -2154),
	Vector(-13483.815430, -6063.548828, -2154),
	Vector(-13343.225586, -6064.234375, -2154),
	Vector(-13221.340820, -6055.854004, -2154),
	Vector(-13087.227539, -6071.311523, -2154)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Class D"
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--todo
end

function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:GiveAmmo( 256, 	"Pistol", 		true )
	self.Player:Give( "weapon_fists" )
	self.Player:Give( "rp_hands" )
	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)

end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

function PLAYER:GetHandsModel()

	return { model = "models/weapons/c_arms_cstrike.mdl", skin = 1, body = "0100000" }

end

player_manager.RegisterClass( "player_citizen", PLAYER, "player_default" )