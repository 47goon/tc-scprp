DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/scp096.mdl"
}

local spawns = 
{
	Vector(-8877.600586, -3306.121826, -2200)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Scp 096"
PLAYER.maxPlayers = 1
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.MaxHealth			= 99999999		-- Max health we can have
PLAYER.StartHealth			= 99999999
PLAYER.doClassNet = true

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end

function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "rp_096" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)

	self.Player:SetHealth(PLAYER.StartHealth)
	self.Player:SetMaxHealth(PLAYER.MaxHealth)

end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_scp_096", PLAYER, "player_default" )