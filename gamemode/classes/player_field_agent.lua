DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/pmc_4/pmc__07.mdl"
}

local spawns = 
{
	Vector(-5158.008301, -384.542389, -1727.968750),
	Vector(-5153.559570, -744.050293, -1727.968750),
	Vector(-5148.584961, -1142.795044, -1727.968750),
	Vector(-4350.875488, -1159.326294, -1727.968750),
	Vector(-4371.250488, -377.796295, -1727.968750)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Field Agent"
PLAYER.maxPlayers = 2
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "M9k_svu" )
	self.Player:GiveAmmo( 100, "ar2", true )

	self.Player:Give( "M9k_l85" )
	self.Player:GiveAmmo( 100, "ar2", true )

	self.Player:Give( "M9k_hk45" )
	self.Player:GiveAmmo( 100, "pistol", true )

	self.Player:Give( "weapon_cuff_police" )
	self.Player:Give( "weapon_leash_rope" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_field_agent", PLAYER, "player_default" )