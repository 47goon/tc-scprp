DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/Zombie/Poison.mdl"
}

local spawns = 
{
	Vector(-8445.023438, -4797.945313, -1514.968750)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "049-2"
PLAYER.maxPlayers = 4
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.MaxHealth			= 100		-- Max health we can have
PLAYER.StartHealth			= 100
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end



function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "m9k_knife" )
	self.Player:Give( "weapon_fists" )
	
	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	self.Player:SetHealth(PLAYER.StartHealth)
	self.Player:SetMaxHealth(PLAYER.MaxHealth)
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_scp_049_minion", PLAYER, "player_default" )