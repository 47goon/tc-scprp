DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/vinrax/player/mtf_guard_player.mdl"
}

local spawns = 
{
	Vector(-11383.665039, -4686.740234, -2145)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Mtf Leader"
PLAYER.maxPlayers = 1
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "m9k_m16a4_acog" )
	self.Player:GiveAmmo( 100, "ar2", true )

	self.Player:Give( "m9k_deagle" )
	self.Player:GiveAmmo( 100, "pistol", true )

	self.Player:Give( "m9k_knife" )

	self.Player:Give( "m9k_spas12" )
	self.Player:GiveAmmo( 100, "buckshot", true )

	self.Player:Give( "weapon_cuff_police" )
	
	self.Player:Give( "weapon_leash_rope" )

	self.Player:Give( "weapon_medkit" )
	

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_mtf_leader", PLAYER, "player_default" )