DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/guard_pack/guard_01.mdl",
	"models/player/guard_pack/guard_02.mdl",
	"models/player/guard_pack/guard_03.mdl",
	"models/player/guard_pack/guard_04.mdl",
	"models/player/guard_pack/guard_05.mdl",
	"models/player/guard_pack/guard_06.mdl",
	"models/player/guard_pack/guard_07.mdl",
	"models/player/guard_pack/guard_08.mdl",
	"models/player/guard_pack/guard_09.mdl",
	
}

local spawns = 
{
	Vector(-12661.340820, -6064.407227, -2144),
	Vector(-12384.556641, -5930.336914, -2144),
	Vector(-12347.145508, -6051.467773, -2144),
	Vector(-12442.131836, -6084.829102, -2144)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Security"
PLAYER.maxPlayers = 2
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "m9k_hk45" )
	self.Player:GiveAmmo( 100, "pistol", true )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_security", PLAYER, "player_default" )	