DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/vinrax/props/scp035/035_mask.mdl"
}

local spawns = 
{
	Vector( -8847.193359, -5887.280273, -2134.968750)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Scp 035"
PLAYER.maxPlayers = 1
PLAYER.WalkSpeed 			= 50
PLAYER.RunSpeed				= 100
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.MaxHealth			= 100		-- Max health we can have
PLAYER.StartHealth			= 100
PLAYER.doClassNet = true

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	
end

function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	--self.Player:Give( "weapon_crowbar" )
	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)

	self.Player:SetHealth(PLAYER.StartHealth)
	self.Player:SetMaxHealth(PLAYER.MaxHealth)
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_scp_035", PLAYER, "player_default" )