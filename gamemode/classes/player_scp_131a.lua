DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/scp-131-a/scp131a.mdl"
}

local spawns = 
{
	Vector( -12748.935547, -4125.033691, -2262.968750)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Scp 131a"
PLAYER.maxPlayers = 1
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 225
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.MaxHealth			= 99999999		-- Max health we can have
PLAYER.StartHealth			= 99999999
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	
end

function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	--self.Player:Give( "weapon_crowbar" )
	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)

	self.Player:SetHealth(PLAYER.StartHealth)
	self.Player:SetMaxHealth(PLAYER.MaxHealth)
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_scp_131a", PLAYER, "player_default" )