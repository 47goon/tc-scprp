DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/pmc_5/pmc__04.mdl"
}

local spawns = 
{
	Vector(-8255.410156, -3926.096191, -2200),
	Vector(-8249.266602, -3717.223633, -2200)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Biohazard Containment Unit"
PLAYER.maxPlayers = 2
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "m9k_m3" )
	self.Player:GiveAmmo( 100, "buckshot", true )

	self.Player:Give( "m9k_deagle" )
	self.Player:GiveAmmo( 100, "pistol", true )

	self.Player:Give( "weapon_medkit" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_bio", PLAYER, "player_default" )