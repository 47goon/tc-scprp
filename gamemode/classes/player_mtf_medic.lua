DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/kerry/class_securety_2.mdl"
}

local spawns = 
{
	Vector(-11383.665039, -4686.740234, -2145)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Mtf Medic"
PLAYER.maxPlayers = 2
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "m9k_vector" )
	self.Player:GiveAmmo( 100, "smg", true )

	self.Player:Give( "m9k_m92beretta" )
	self.Player:GiveAmmo( 100, "pistol", true )

	self.Player:Give( "weapon_medkit" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_mtf_medic", PLAYER, "player_default" )