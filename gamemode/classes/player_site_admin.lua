DEFINE_BASECLASS( "player_default" )
AddCSLuaFile()

local PLAYER = {}

local models = 
{
	"models/player/suits/male_01_open_tie.mdl",
	"models/player/suits/male_02_open_tie.mdl",
	"models/player/suits/male_03_open_tie.mdl",
	"models/player/suits/male_04_open_tie.mdl",
	"models/player/suits/male_05_open_tie.mdl",
	"models/player/suits/male_06_open_tie.mdl",
	"models/player/suits/male_07_open_tie.mdl",
	"models/player/suits/male_08_open_tie.mdl",
	"models/player/suits/male_09_open_tie.mdl"
	
}

local spawns = 
{
	Vector(-9366.096680, -1728.726074, -2120)
}
--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.class_displayName = "Site Admin"
PLAYER.maxPlayers = 1
PLAYER.WalkSpeed 			= 100
PLAYER.RunSpeed				= 275
PLAYER.CrouchedWalkSpeed	= 0.1
PLAYER.doClassNet = false

PLAYER.Spawn = function(tbl)
	return spawns
end


function PLAYER:Init()
	--TODO
end
--m9k AMMOS ["smg","buckshot","winchester","sniper_rounds", "ar2", "pistol"]
function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()
	self.Player:StripWeapons()

	self.Player:Give( "weapon_fists" )
	self.Player:Give( "rp_hands" )

	self.Player:SetWalkSpeed(self.WalkSpeed)
	self.Player:SetRunSpeed(self.RunSpeed)
	
end

function PLAYER:SetModel()
	local m = models[math.Round(math.Rand(1,#models), 0)]
	util.PrecacheModel( m )
	self.Player:SetModel( m )
end

player_manager.RegisterClass( "player_site_admin", PLAYER, "player_default" )	