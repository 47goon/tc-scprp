AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include( "shared.lua" )
include("sv_job.lua")

AddCSLuaFile( "cl_scp_f4menu.lua" )
AddCSLuaFile( "cl_jobText.lua" )

--FILES
resource.AddFile("materials/vgui/scpLOGOW.png")
resource.AddFile("resource/fonts/CaviarDreams.ttf")
resource.AddFile("resource/fonts/CaviarDreams_Bold.ttf")
resource.AddFile("resource/fonts/CaviarDreams_BoldItalic.ttf")
resource.AddFile("resource/fonts/CaviarDreams_Italic.ttf")
resource.AddFile("resource/fonts/Sullivan.ttf")

resource.AddFile( "sound/scp_049/1.mp3" ) 
resource.AddFile( "sound/scp_049/2.mp3" ) 
resource.AddFile( "sound/scp_049/3.mp3" ) 
resource.AddFile( "sound/scp_049/4.mp3" ) 

resource.AddFile( "sound/weapons/scp682/roar.wav" ) 
resource.AddFile( "sound/weapons/scp682/bite.wav" )
resource.AddFile( "sound/weapons/scp682/growl.wav" )
--FILES

--WORKSHOP
resource.AddWorkshop( '268195264' ) -- SCP 035 Player and NPC model
resource.AddWorkshop( '183901628' ) -- SCP 049 Player and NPC model
resource.AddWorkshop( '166259429' ) -- SCP 096 from SCP - Containment Breach
resource.AddWorkshop( '268641868' ) -- SCP Containment Breach Prop Pack
resource.AddWorkshop( '165104626' ) -- SCP 173 from SCP - Containment Breach
resource.AddWorkshop( '280961678' ) -- SCP 106 Player and NPC
resource.AddWorkshop( '179920842' ) -- SCP 096 swep
resource.AddWorkshop( '164804857' ) -- SCP 106 from SCP - Containment Breach
resource.AddWorkshop( '740748927' ) -- SCP Staff
resource.AddWorkshop( '827243834' ) -- SCP 173 Player Model
resource.AddWorkshop( '759566204' ) -- SCP Guard - Playermodel
resource.AddWorkshop( '169011381' ) -- Security Guard Playermodels
resource.AddWorkshop( '128089118' ) -- M9K Assault Rifles
resource.AddWorkshop( '128091208' ) -- M9K Heavy Weapons
resource.AddWorkshop( '128093075' ) -- M9K Small Arms pack
resource.AddWorkshop( '144982052' ) -- M9K Specialties
resource.AddWorkshop( '299279882' ) -- Murderthon 9000 Administration
resource.AddWorkshop( '231961544' ) -- Missing .WAV files for M9K
resource.AddWorkshop( '543020570' ) -- Terminus SCP:RP Server Content
resource.AddWorkshop( '543021477' ) -- Terminus SCP:RP Materials Part 1
resource.AddWorkshop( '543016455' ) -- Terminus SCP:RP Materials Part 2
resource.AddWorkshop( '543019081' ) -- Terminus SCP:RP Sounds
resource.AddWorkshop( '551074297' ) -- Terminus - SCP:RP Map
resource.AddWorkshop( '915895321' ) -- SCP 035 ENTITY
resource.AddWorkshop( '181917561' ) -- MTF Guard Player and NPC model
resource.AddWorkshop( '412149725' ) -- suits and robbers playermodels
resource.AddWorkshop( '905114757' ) -- SCP-682 PM
resource.AddWorkshop( '774064122' ) -- SCP-294 ENTITY
resource.AddWorkshop( '548140912' ) -- OF2 - Phoenix Marines
resource.AddWorkshop( '426985804' ) -- SCP 096 Player Model
resource.AddWorkshop( '871969365' ) -- PMC
--WORKSHOP

for k, v in pairs(file.Find("scprp/gamemode/class_abilities/*.lua","LUA")) do AddCSLuaFile("class_abilities/" .. v) end
for k, v in pairs(file.Find("scprp/gamemode/client/*.lua","LUA")) do AddCSLuaFile("client/" .. v) end

--MODULE SYSTEM CREDI TO DARKRP SOURCE CODE
local fol = GM.FolderName .. "/gamemode/modules/"
local files, folders = file.Find(fol .. "*", "LUA")

for _, folder in pairs(folders) do
	for _, File in pairs(file.Find(fol .. folder .. "/cl_*.lua", "LUA"), true) do
        AddCSLuaFile(fol .. folder .. "/" .. File)
    end

    for _, File in pairs(file.Find(fol .. folder .. "/sv_*.lua", "LUA"), true) do
        include(fol .. folder .. "/" .. File)
    end

    for _, File in pairs(file.Find(fol .. folder .. "/sh_*.lua", "LUA"), true) do
        AddCSLuaFile(fol .. folder .. "/" .. File)
        include(fol .. folder .. "/" .. File)
    end
end

timer.Simple(30, function()
	RunConsoleCommand("hostname", "[US] Offical Terminus" .. SCP_VERSION .. "[No-Lag][FastDL]")
end)


function GM:PlayerSelectSpawn(ply)
	local spawns = ents.FindByClass( "info_player_start" )
	local random_entry = math.random( #spawns )

	local ply_spawns = player_manager.RunClass( ply, "Spawn" )
	if( ply_spawns ) then
		ply:SetPos(ply_spawns[math.random(#ply_spawns)]) 
	else
		return spawns[ random_entry ]		
	end
	
end

function GM:PlayerInitialSpawn(ply)
	ply:SetNWInt("ply_ability", 100)
	player_manager.SetPlayerClass( ply, "player_citizen")
	
	hook.Call("PlayerLoadout",self, ply)
	hook.Call("PlayerSetModel", self, ply)

	local ent, pos = hook.Call("PlayerSelectSpawn", self, ply)
    if(ent or pos) then ply:SetPos(pos or ent:GetPos()) end
    --ply:ConCommand("scp_opem_motd")
end


function GM:PlayerSpawn(ply)
	ply.shouldFreeze = false
	ply:SetNWInt("ply_ability", 100)
	ply:SetCustomCollisionCheck( true )
	hook.Call("PlayerLoadout",self, ply)
	hook.Call("PlayerSetModel", self, ply)

	local ent, pos = hook.Call("PlayerSelectSpawn", self, ply)
    if(ent or pos) then ply:SetPos(pos or ent:GetPos()) end

   
	local oldhands = ply:GetHands()
  if ( IsValid( oldhands ) ) then oldhands:Remove() end

  local hands = ents.Create( "gmod_hands" )
  if ( IsValid( hands ) ) then
    ply:SetHands( hands )
    hands:SetOwner( ply )

    -- Which hands should we use?
    local cl_playermodel = ply:GetInfo( "cl_playermodel" )
    local info = player_manager.TranslatePlayerHands( cl_playermodel )
    if ( info ) then
      hands:SetModel( info.model )
      hands:SetSkin( info.skin )
      hands:SetBodyGroups( info.body )
    end

    -- Attach them to the viewmodel
    local vm = ply:GetViewModel( 0 )
    hands:AttachToViewmodel( vm )

    vm:DeleteOnRemove( hands )
    ply:DeleteOnRemove( hands )

    hands:Spawn()

    
  end
end

function GM:ShowSpare2(player)
	player:ConCommand("scp_f4menu_toggle");
end

function GM:PlayerNoClip(ply, desi)
	if(ply:Nick() == "47goon" or ply:Nick() == "Zexos") then return true end
	return true
end



function GM:CanPlayerSuicide( ply )
	return false
end


function GM:PlayerTick(ply, mv)
	if(ply:GetNWInt("ply_ability") < 100 and delay and CurTime() >= delay or !delay) then
		ply:SetNWInt("ply_ability", ply:GetNWInt("ply_ability") + 5)
		delay = CurTime() + 1
	end
	
end

--models/Items/ammocrate_ar2.mdl

function GM:PlayerSwitchFlashlight( ply, bool )
	return true
end

function GM:PlayerDeathSound() 
	return true
end




hook.Add("PlayerDeath", "scp_049_minion_reset", function(ply, inf, attack)
	if(player_manager.GetPlayerClass(ply) == "player_scp_049_minion" or player_manager.GetPlayerClass(ply) == "player_scp_008_zombie") then
		if (ply.last_job) then
			print("LAST JOB FOUND")
			player_manager.SetPlayerClass(ply, ply.last_job)
			ply:Spawn()
			ply.last_job = nil
			return
		end
		player_manager.SetPlayerClass(ply, "player_citizen")
		ply:Spawn()
	end
end)

hook.Add( "PlayerCanHearPlayersVoice", "Maximum Range", function( listener, talker )
	if listener:GetPos():Distance( talker:GetPos() ) > 500 then return false end
end )


function GM:PlayerCanSeePlayersChat( str, teamOnly, listener, talker )
	
	local strText = string.lower(str)
	if(string.sub(strText, 1, 4) == "/ooc" or string.sub(strText, 1, 2) == "//") then return true end
	if(string.sub(strText, 1, 7) == "/advert") then return true end
	
	if listener:GetPos():DistToSqr(talker:GetPos()) < 100000 then 
		return true 
	end
	
end

hook.Add( "PlayerSay", "doAmmoRestock", function( ply, text, team )
	text = string.lower( text )
	if ( text == "/ammo" ) then -- if the first four characters of the string are /all
		ply:EmitSound( "items/ammo_pickup.wav" )
		player_manager.RunClass( ply, "Loadout" )
		return ""
	end
end )




hook.Add( "PlayerUse", "stoppDoors", function( ply, ent )
	if (IsValid(ent) and isScp( player_manager.GetPlayerClass(ply)) ) then
		return false
	end
end )

