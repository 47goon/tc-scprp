if (SERVER) then
	SCP_MASK_ENT = SCP_MASK_ENT or nil
	function load_035(pos)
		for k,v in pairs(ents.FindByClass("scp_035")) do
			v:Remove()
		end
		local mask = ents.Create("scp_035")

		if (pos) then   
			mask:SetPos(pos)
		else
			mask:SetPos(Vector(-8843.473633, -5792.809570, -2140))
		end
		
		mask:Spawn()
		mask:PhysicsInit(SOLID_VPHYSICS)

		local phys = mask:GetPhysicsObject()
		phys:EnableGravity(true)
		phys:Wake()
		SCP_MASK_ENT = mask
		SCP_MASK_ENT.Owner = nil
	end
	
	util.AddNetworkString("mask_send_cl")
	hook.Add( "InitPostEntity", "init_mask_ent", function()
		load_035()
	end)

	hook.Add("PlayerDeath", "DropMask", function(ply)
	    if (player_manager.GetPlayerClass(ply) == "player_scp_035") then
	        load_035(ply:GetPos())
	    end
	end)

	hook.Add("PlayerSpawn", "DropMask", function(ply)
	    if (player_manager.GetPlayerClass(ply) == "player_scp_035") then
	        ply:Spectate( OBS_MODE_CHASE )
			ply:SpectateEntity( SCP_MASK_ENT )
			SCP_MASK_ENT.Owner = ply
			ply:StripWeapons()
	    end
	end)

	
end
if (CLIENT) then
	net.Receive("mask_send_cl", function(len, ply)
		
	end)
end

