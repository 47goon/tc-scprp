SCP_BUILD_SV = {}
SCP_BUILD_SV["Props"] = {}
SCP_BUILD_SV["Entites"] = {}
SCP_BUILD_SV["Weapons"] = {}
SCP_BUILD_SV["Other"] = {}

SCP_BUILD_SV.SCP_PROP_COUNT = 0
SCP_BUILD_SV.SCP_OTHER_COUNT = 0
--[[---------------------------------------------------------------------------
	PROPS
---------------------------------------------------------------------------]]
local function registerProp(model, name, vars)
	SCP_BUILD_SV.SCP_PROP_COUNT = SCP_BUILD_SV.SCP_PROP_COUNT + 1
	SCP_BUILD_SV["Props"][SCP_BUILD_SV.SCP_PROP_COUNT] = {}
	SCP_BUILD_SV["Props"][SCP_BUILD_SV.SCP_PROP_COUNT].index = SCP_BUILD_SV.SCP_PROP_COUNT
	SCP_BUILD_SV["Props"][SCP_BUILD_SV.SCP_PROP_COUNT].model = model
	SCP_BUILD_SV["Props"][SCP_BUILD_SV.SCP_PROP_COUNT].name = name 
	SCP_BUILD_SV["Props"][SCP_BUILD_SV.SCP_PROP_COUNT].vars = vars or {}
end
local vars = {}
vars.job = "player_mtf_officer"
registerProp("models/props_wasteland/barricade001a.mdl", "Barricade 1", vars )
registerProp("models/props_wasteland/barricade002a.mdl", "Barricade 2", vars)
registerProp("models/props_c17/concrete_barrier001a.mdl", "Barricade 3", vars)

vars = {}
vars.job = "player_security"
registerProp("models/props_c17/fence01b.mdl", "Basic Fence", vars )
registerProp("models/props_wasteland/controlroom_desk001a.mdl", "Desk", vars)

--[[---------------------------------------------------------------------------
	Entites
---------------------------------------------------------------------------]]
--[[---------------------------------------------------------------------------
	Weapons
---------------------------------------------------------------------------]]
--[[---------------------------------------------------------------------------
	Other
---------------------------------------------------------------------------]]
local function registerOther(name, vars)
	SCP_BUILD_SV.SCP_OTHER_COUNT = SCP_BUILD_SV.SCP_OTHER_COUNT + 1
	SCP_BUILD_SV["Other"][SCP_BUILD_SV.SCP_OTHER_COUNT] = {}
	SCP_BUILD_SV["Other"][SCP_BUILD_SV.SCP_OTHER_COUNT].name = name 
end
registerOther("Remove Tool")
registerOther("Remove all props")
--[[---------------------------------------------------------------------------
	NETWORKING
---------------------------------------------------------------------------]]
util.AddNetworkString("request_buildTable")
util.AddNetworkString("send_buildTable")
net.Receive("request_buildTable", function(len, ply)
	MsgC( Color( 255, 255, 0 ), "\n" .. ply:Nick(), Color( 0, 255, 0 ), " Has requested the build table \n")
	net.Start("send_buildTable")
		net.WriteTable(SCP_BUILD_SV)
	net.Send(ply)
	ply:SetNWBool("hasBuildTable", true)
end)

--[[---------------------------------------------------------------------------
	HOOKS
---------------------------------------------------------------------------]]
local function propTableInit( ply )
	ply.propTbl = {}
end
hook.Add( "PlayerInitialSpawn", "InitPropTable", propTableInit )

local function propTableDestroy( ply )
	for k,v in pairs(ply.propTbl) do
		if (IsValid(v)) then
			v:Remove()
		end
	end
end
hook.Add( "PlayerDisconnected", "DestroyPropTable", propTableDestroy )

hook.Add( "PropBreak", "RemovePropOnBreak", function( client, prop )
	if (prop.Owner) then
		for k,v in pairs(prop.Owner.propTbl) do
			if (v == prop) then
				table.RemoveByValue( prop.Owner.propTbl, prop )
			end
		end
	end
end )