

util.AddNetworkString("scp_note")
util.AddNetworkString("scpBUILD")
net.Receive("scpBUILD", function(len, net_ply)
	if (#net_ply.propTbl >= 5) then net.Start("scp_note") net.WriteString("Max prop limit has been reached") net.Send(net_ply) return end
	local tr = util.TraceLine( {
	start = net_ply:EyePos() ,
	endpos = net_ply:EyePos() + net_ply:EyeAngles():Forward() * 250,
	filter = function( ent ) if ( ent:IsPlayer() ) then return false end if ( ent:GetClass() == "func_door" ) then return true end end
	} )
	local pos = tr.HitPos
	local ang = net.ReadAngle()
	local model = net.ReadString()
	local holeBarr = ents.Create("prop_physics")
	holeBarr:SetModel(model)
	holeBarr:SetPos( pos )
	holeBarr:SetPos(Vector(holeBarr:GetPos().x, holeBarr:GetPos().y, net_ply:GetPos().z))
	holeBarr:SetPos(Vector(holeBarr:GetPos().x, holeBarr:GetPos().y, holeBarr:GetPos().z - holeBarr:OBBMins().z))
	holeBarr:SetAngles( ang )
	

	tr = {
	start = holeBarr:GetPos(),
	endpos = holeBarr:GetPos(),
	mins = holeBarr:OBBMins(),
	maxs = holeBarr:OBBMaxs(),
	filter = function( ent ) 
		if ( ent == holebarr ) then return false end 
		if ( ent:GetClass() == "prop_physics" ) then return true end 
		if ( ent:IsPlayer() ) then return true end 
		if ( ent:IsPlayer() and ent == LocalPlayer() ) then return true end 
	end
	}

	local hullTrace = util.TraceHull(tr)
	
	if(hullTrace.Hit) then
		holeBarr:Remove()
	end

	tr = util.TraceLine( {
			start = holeBarr:GetPos() ,
			endpos = (holeBarr:GetPos() - Vector(0,0,holeBarr:OBBMaxs().z)) + -holeBarr:GetUp() * 5,
	} )
	if (!tr.Hit) then
		holeBarr:Remove()
	end

	holeBarr:Spawn()
	if (IsValid(holeBarr)) then table.insert( net_ply.propTbl, holeBarr ) end
	holeBarr.Owner = net_ply
	

	local phys = holeBarr:GetPhysicsObject()
	if phys and phys:IsValid() then
		phys:EnableMotion(false) -- Freezes the object in place.
	end
end)



--BUILD
util.AddNetworkString("request_buildMode")
net.Receive("request_buildMode", function(len, net_ply)
	local model = net.ReadString()
	local index = net.ReadFloat()
	if(SCP_BUILD_SV["Props"][index].model != model) then return end
	if (SCP_BUILD_SV["Props"][index].vars) then
		if (SCP_BUILD_SV["Props"][index].vars.job) then
			if (SCP_BUILD_SV["Props"][index].vars.job != player_manager.GetPlayerClass(net_ply)) then return end
		end
	end
	

	net_ply:Give( "rp_build" )
	net_ply:SelectWeapon( "rp_build" )
	net_ply:SetNWString("propModel", model)
end)

util.AddNetworkString("request_Other")
net.Receive("request_Other", function(len, net_ply)
	local name = net.ReadString()
	if (name == "Remove Tool") then
		local ent = net_ply:GetEyeTrace().Entity
		if (!IsValid(ent) or !ent:GetClass("prop_physics")) then return end
		if (serverguard.player:GetImmunity(net_ply) >= 75) then
			for k,v in pairs(ent.Owner.propTbl) do
				if (v == ent) then
					table.RemoveByValue( ent.Owner.propTbl, ent )
				end
			end
			ent:Remove()
			return
		end
		if (ent.Owner == net_ply) then
			for k,v in pairs(ent.Owner.propTbl) do
				if (v == ent) then
					table.RemoveByValue( ent.Owner.propTbl, ent )
				end
			end
			ent:Remove()
		end
	end
	if (name == "Remove all props") then
		for k,v in pairs(net_ply.propTbl) do
			if (IsValid(v)) then
				v:Remove()
			end
			net_ply.propTbl = {}
		end
	end
end)