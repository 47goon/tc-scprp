local SCP_BUILD_MENU = SCP_BUILD_MENU or {}



local SCP_BUILD_CL = nil
net.Receive("send_buildTable", function(len,  ply)
	SCP_BUILD_CL = net.ReadTable()
end)

hook.Add ("Think", "loadbuild", function ()
    if IsValid (LocalPlayer()) then
        net.Start("request_buildTable")
		net.SendToServer()
        hook.Remove ("Think", "loadbuild")
    end
end)

local tabs = {"Props" , "Entites", "Weapons", "Other"}
local currentTab = "Props"
SCP_BUILD_MENU.tabWidth = 245
SCP_BUILD_MENU.tabHeigt = 60


SCP_BUILD_MENU.holderPad = 25
local tabPadding = SCP_BUILD_MENU.holderPad/2

SCP_BUILD_MENU.init = false

--[[---------------------------------------------------------------------------
BUILD BACKGROUND
---------------------------------------------------------------------------]]

local blur = Material("pp/blurscreen")
local function DrawBlur( p, a, d )
	local x, y = p:LocalToScreen(0, 0)
	surface.SetDrawColor( 255, 255, 255 )
	surface.SetMaterial( blur )
	for i = 1, d do
		blur:SetFloat( "$blur", (i / d ) * ( a ) )
		blur:Recompute()
		render.UpdateScreenEffectTexture()
		surface.DrawTexturedRect( x * -1, y * -1, ScrW(), ScrH() )
	end
end

local PANEL = {}
function PANEL:Init()
	self:SetSize( ScrW() * 0.65, ScrH() * 0.75 )

end

function PANEL:Paint( w, h )
	DrawBlur(self,3,6)
	draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 125 ) )
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end
vgui.Register( "buildBG", PANEL, "Panel" )

--[[---------------------------------------------------------------------------
BUILD TAB
---------------------------------------------------------------------------]]
local PANEL = {}
function PANEL:Init()
	self:SetSize( SCP_BUILD_MENU.tabWidth, SCP_BUILD_MENU.tabHeigt )
	self.text = "NULL TAB"
	self.moveBy = 25
	self.orgX, self.orgY = self:GetPos()

	self.button = vgui.Create( "DButton", self )
	self.button:SetText("")
	self.button:SetPos(0,0)
	self.button:SetSize(self:GetWide(), self:GetTall())
	self.button.Paint = function(self)
		draw.RoundedBox( 0, 0, 0, self:GetWide(), self:GetTall(), Color( 0, 0, 0, 0 ) )
	end

end

function PANEL:Paint( w, h )
	surface.SetFont( "scp_caviar_info" )
	local w_t , h_t = surface.GetTextSize(self.text)
	surface.SetTextColor( 255, 255, 255, 255 )
	surface.SetTextPos( self:GetWide()*0.5 - w_t/2, self:GetTall()*0.5 - h_t/2 )
	
	if(self:IsHovered() or self.button:IsHovered()) then
		surface.DrawText( self.text )
		draw.RoundedBox( 5, 0, 0, w, h, Color( 127, 140, 141, 25 ) )
		local x,y = self:GetPos()
		if(x > self.orgX) then
			return
		end
		if(x > self.orgX + self.moveBy) then
			return
		end
		self:MoveTo( x + self.moveBy, y, 0.1, 0, 0.1)
	else
		surface.DrawText( self.text )
		draw.RoundedBox( 5, 0, 0, w, h, Color( 127, 140, 141, 10 ) )
		local x,y = self:GetPos()
		if(x >= self.orgX + self.moveBy) then
			self:MoveTo( self.orgX, self.orgY, 0.1, 0, 0.4)
		end
	end
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "buildTab", PANEL, "Panel" )


--[[---------------------------------------------------------------------------
LABEL
---------------------------------------------------------------------------]]
local PANEL = {}
function PANEL:Init()
	self:SetSize( 500, 500 )
end

function PANEL:Paint( w, h )
	surface.SetFont( "scp_caviar_info" )
	local w_t , h_t = surface.GetTextSize(self.text)
	surface.SetTextColor( 255, 255, 255, 255 )
	surface.SetTextPos( self:GetWide()*0.5 - w_t/2, self:GetTall()*0.5 - h_t/2 )
	surface.DrawText( self.text )
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end
vgui.Register( "buildLabel", PANEL, "Panel" )


--[[---------------------------------------------------------------------------
MAIN BACKGROUND
---------------------------------------------------------------------------]]
function SCP_BUILD_MENU:Init()
	self.init = true
	SCP_BUILD_MENU.BG = vgui.Create("buildBG")
	SCP_BUILD_MENU.BG:Show()
	SCP_BUILD_MENU.BG:Center()

	SCP_BUILD_MENU.tabBg = vgui.Create("buildBG", SCP_BUILD_MENU.BG)
	SCP_BUILD_MENU.tabBg:SetSize(SCP_BUILD_MENU.tabWidth + 25, SCP_BUILD_MENU.BG:GetTall() )
	

	SCP_BUILD_MENU.holder = vgui.Create("buildBG", SCP_BUILD_MENU.BG)
	SCP_BUILD_MENU.holder:SetSize( SCP_BUILD_MENU.BG:GetWide() - SCP_BUILD_MENU.tabBg:GetWide() - SCP_BUILD_MENU.holderPad, SCP_BUILD_MENU.BG:GetTall() - SCP_BUILD_MENU.holderPad )
	SCP_BUILD_MENU.holder:Center()
	local x,y = SCP_BUILD_MENU.holder:GetPos()
	SCP_BUILD_MENU.holder:SetPos(x + SCP_BUILD_MENU.tabBg:GetWide()/2 ,y)

	SCP_BUILD_MENU.headLabel = vgui.Create( "buildLabel", SCP_BUILD_MENU.holder )
	SCP_BUILD_MENU.headLabel:SetSize(SCP_BUILD_MENU.holder:GetWide(), 75)
	SCP_BUILD_MENU.headLabel:Center()
	x,y = SCP_BUILD_MENU.headLabel:GetPos()
	SCP_BUILD_MENU.headLabel:SetPos(x, 10)
	SCP_BUILD_MENU.headLabel.Paint = function(self)
		surface.SetFont( "scp_caviar_info" )
		local w_t , h_t = surface.GetTextSize("Your avaliable " .. currentTab)
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( self:GetWide()*0.5 - w_t/2, 0 )
		surface.DrawText( "Your avaliable " .. currentTab )
	end

	SCP_BUILD_MENU.listHolder = vgui.Create("buildBG", SCP_BUILD_MENU.holder)
	SCP_BUILD_MENU.listHolder:SetSize(SCP_BUILD_MENU.holder:GetWide() * 0.95, SCP_BUILD_MENU.holder:GetTall() *0.95 - 75)
	SCP_BUILD_MENU.listHolder:SetPos(SCP_BUILD_MENU.holder:GetWide()*0.5 -SCP_BUILD_MENU.listHolder:GetWide()/2 , 75)

	SCP_BUILD_MENU.scroll = vgui.Create( "DScrollPanel", SCP_BUILD_MENU.listHolder ) 
	SCP_BUILD_MENU.scroll:SetSize(SCP_BUILD_MENU.listHolder:GetWide(), SCP_BUILD_MENU.listHolder:GetTall())
	SCP_BUILD_MENU.scroll:SetPos(0,0)
	
	SCP_BUILD_MENU.list = vgui.Create( "DIconLayout", SCP_BUILD_MENU.scroll )
	SCP_BUILD_MENU.list:SetSize( SCP_BUILD_MENU.scroll:GetWide(), SCP_BUILD_MENU.scroll:GetTall() )
	SCP_BUILD_MENU.list:SetPos( 0, 0 )
	SCP_BUILD_MENU.list:SetSpaceY( 5 ) 
	SCP_BUILD_MENU.list:SetSpaceX( 5 ) 

	SCP_BUILD_MENU.list.refill = function(self)
		SCP_BUILD_MENU.list:Clear()
		for k,v in pairs(SCP_BUILD_CL[currentTab]) do
			if (SCP_BUILD_CL[currentTab][k].vars != nil and SCP_BUILD_CL[currentTab][k].vars.job ) then 
				if (SCP_BUILD_CL[currentTab][k].vars.job != player_manager.GetPlayerClass( LocalPlayer() ) ) then
					continue
				end 
			end
			local ListItem = SCP_BUILD_MENU.list:Add( "DPanel" ) 
			ListItem:SetSize( SCP_BUILD_MENU.list:GetWide(), 100 ) 
			ListItem.opacity = 125
			ListItem.button = vgui.Create( "DButton", ListItem )
			ListItem.button:SetSize(ListItem:GetWide(), ListItem:GetTall())
			ListItem.button:SetPos(0,0)
			ListItem.button:SetText("")
			ListItem.button.Paint = function(self)
				if(self:IsHovered()) then
					ListItem.opacity = 50
				else
					ListItem.opacity = 125
				end
				draw.RoundedBox( 0, 0, 0, self:GetWide(), self:GetTall(), Color( 0, 0, 0, 0 ) )
			end

			ListItem.button.DoClick = function()
				SCP_BUILD_MENU:hide()
				if (currentTab == "Props") then
					net.Start("request_buildMode")
						net.WriteString(SCP_BUILD_CL[currentTab][k].model)
						net.WriteFloat(SCP_BUILD_CL[currentTab][k].index)
					net.SendToServer()
				end

				if (currentTab == "Other") then
					net.Start("request_Other")
						net.WriteString(SCP_BUILD_CL[currentTab][k].name)
					net.SendToServer()
				end
				
			end
		

			ListItem.modelPanel = vgui.Create( "DModelPanel", ListItem)
			ListItem.modelPanel:SetPos(0,0)
			ListItem.modelPanel:SetSize(100,100)
			ListItem.modelPanel:SetModel(SCP_BUILD_CL[currentTab][k].model or "")
			ListItem.Paint = function(self)
				draw.RoundedBox( 0, 0, 0, self:GetWide(), self:GetTall(), Color( 0, 0, 0, ListItem.opacity ) )
				surface.SetFont( "scp_caviar_info" )
				local w_t , h_t = surface.GetTextSize(SCP_BUILD_CL[currentTab][k].name)
				surface.SetTextColor( 255, 255, 255, 255 )
				surface.SetTextPos( self:GetWide()*0.5 - w_t/2, self:GetTall()*0.5 - h_t/2 )
				surface.DrawText(SCP_BUILD_CL[currentTab][k].name)
			end
		
		end	
	end
	SCP_BUILD_MENU.list.refill()

	for k,v in pairs(tabs) do
		local tab = vgui.Create("buildTab", SCP_BUILD_MENU.BG)
		tab:SetPos(10 , tabPadding)
		tab.orgX, tab.orgY = tab:GetPos()
		tabPadding = tabPadding + SCP_BUILD_MENU.tabHeigt + 10
		tab.text = v

		tab.button.DoClick = function()
			currentTab = v
			SCP_BUILD_MENU.list.refill()
		end
	end


	

end



function SCP_BUILD_MENU:show()
	if(!self.init ) then SCP_BUILD_MENU:Init() end
	if(SCP_BUILD_MENU.list) then SCP_BUILD_MENU.list.refill() end
	SCP_BUILD_MENU.BG:Show()
	gui.EnableScreenClicker(true)
	
	
end

function SCP_BUILD_MENU:hide()
	SCP_BUILD_MENU.BG:Hide()
	gui.EnableScreenClicker(false)
end


function GM:OnSpawnMenuOpen()
	if(IsValid(LocalPlayer():GetActiveWeapon()) and LocalPlayer():GetActiveWeapon():GetClass() == "rp_build") then return end
	SCP_BUILD_MENU:show()
end

function GM:OnSpawnMenuClose()
	if(IsValid(LocalPlayer():GetActiveWeapon())  and LocalPlayer():GetActiveWeapon():GetClass() == "rp_build") then return end
	SCP_BUILD_MENU:hide()
end

