local delay = 0
local DelayMins = 10
hook.Add( "PlayerUse", "scp_008", function( ply, ent )
	if(ent:GetPos().z == -2150 and player_manager.GetPlayerClass(ply) != "player_citizen") then return false end
	if(ent:GetPos().z == -2150 and player_manager.GetPlayerClass(ply) == "player_citizen") then
		if(delay and delay > CurTime()) then return false end
		local rand = math.random() * 100
		if(rand >= 50) then
			--print("Infected")
			ply:EmitSound("npc/zombie/zombie_alert3.wav")
			ply:StripWeapons()
			player_manager.SetPlayerClass(ply, "player_scp_008_zombie")
			local pos = ply:GetPos()
			ply:Spawn()
			ply:SetPos(pos)
		else
			--print("Not Infected")
		end
		
		delay = CurTime() + DelayMins
		timer.Simple(DelayMins, function()
			ent:Fire("close", 0)
		end)
	end
end )