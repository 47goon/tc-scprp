LocalPlayer().inThirdPerson = false
local camDistance = 100

local ViewHullMins = Vector(-8, -8, -8)
local ViewHullMaxs = Vector(8, 8, 8)
local function MyCalcView( ply, origin, angles, fov )
	local view = {}

	local tr = util.TraceHull({start = origin, endpos = origin - angles:Forward() * camDistance, mask = MASK_SHOT, filter = player.GetAll(), mins = ViewHullMins, maxs = ViewHullMaxs})

	view.origin = tr.HitPos + tr.HitNormal * 3
	view.angles = angles
	view.fov = fov
	view.drawviewer = true

	return view
end
concommand.Add( "scp_toggle_thirdperson", function( ply, cmd, args )
	if(ply.inThirdPerson) then
		hook.Add( "CalcView", "scp_thirdperson", MyCalcView )
	else
		hook.Remove( "CalcView", "scp_thirdperson" )
	end

	ply.inThirdPerson = !ply.inThirdPerson
end )