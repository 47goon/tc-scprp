hook.Add( "KeyPress", "ply_scp_372", function( ply, key )
	if (player_manager.GetPlayerClass(ply) != "player_scp_372") then return end
	if ( key == IN_JUMP and ply:IsOnGround()) then
		ply:SetVelocity(  ply:GetAimVector() * 750  )
	end

	if (key == IN_ATTACK and ply:GetEyeTrace().Entity and !isScp( player_manager.GetPlayerClass(ply:GetEyeTrace().Entity) ) ) then
		if (!ply:GetEyeTrace().Entity:IsPlayer()) then return end
		if (ply:GetEyeTrace().Entity:GetPos():Distance(ply:GetPos()) < 100) then
			ply:GetEyeTrace().Entity:EmitSound( "npc/barnacle/neck_snap2.wav", 90, 100, 1, CHAN_AUTO ) 
			ply:GetEyeTrace().Entity:Kill()

		end
	end
end )