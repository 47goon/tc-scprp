hook.Add("Move", "WallCling", function(pl, move)
    if (player_manager.GetPlayerClass(pl) != "player_scp_372") then return end
    if not pl:Alive() then return end
    if move:KeyPressed(IN_JUMP) and not pl:OnGround() then
        --print("testing")
        local curVel = move:GetVelocity()
        local trace = util.TraceHull({
            start = move:GetOrigin(),
            endpos = move:GetOrigin() + (curVel * FrameTime()),
            filter = pl,
            mins = pl:OBBMins() - Vector(2, 2, -2),
            maxs = pl:OBBMaxs() + Vector(2, 2, 2)
        })
       -- debugoverlay.Box(trace.HitPos, pl:OBBMins(), pl:OBBMaxs(), 3, Color(255, 255, 255, 16))
        if trace.Hit then
            pl.wallClinging = true
            --print("started wall cling")
        end
    end
    if move:KeyDown(IN_JUMP) and pl.wallClinging == true then
        local curVel = move:GetVelocity()
        local trace2 = util.TraceHull({
            start = move:GetOrigin(),
            endpos = move:GetOrigin() + (curVel * FrameTime()),
            filter = pl,
            mins = pl:OBBMins() - Vector(2, 2, -2),
            maxs = pl:OBBMaxs() + Vector(2, 2, 2)
        })
        if trace2.Hit then
            --print("continuing wall cling")
            pl.wallClinging = true
            move:SetVelocity(Vector(0,0,300*FrameTime()))
            --pl:SetGravity(0.001)
        end
    else
        pl.wallClinging = false
        --pl:SetGravity(1)
    end
end)


hook.Add( "KeyPress", "ply_scp_372_sh", function( ply, key )
    if (player_manager.GetPlayerClass(ply) != "player_scp_372") then return end
    if (ply.inCloak == nil) then ply.inCloak = false end
    if (key == IN_RELOAD and (!ply.cloakDelay or ply.cloakDelay <= CurTime()) ) then
        if (ply.inCloak) then
            ply:SetNoDraw(true)
        else
            ply:SetNoDraw(false)
        end
        ply.inCloak = !ply.inCloak
        ply.cloakDelay = CurTime() + 1
    end
end )

if (CLIENT) then
    local tab = {
        ["$pp_colour_addr"] = 0,
        ["$pp_colour_addg"] = 0,
        ["$pp_colour_addb"] = 0.05,
        ["$pp_colour_brightness"] = 0,
        ["$pp_colour_contrast"] = 1,
        ["$pp_colour_colour"] = 1,
        ["$pp_colour_mulr"] = 0,
        ["$pp_colour_mulg"] = 0,
        ["$pp_colour_mulb"] = 0
    }

    hook.Add( "RenderScreenspaceEffects", "ply_scp_372_sh_screen", function()
        if (!LocalPlayer().inCloak and player_manager.GetPlayerClass(LocalPlayer())  == "player_scp_372" ) then
            tab["$pp_colour_addb"] = math.sinwave( 2, 0.1, true )
            DrawColorModify( tab ) --Draws Color Modify effect
            DrawMotionBlur( 0.4, 0.8, 0.01 )

        end
    end )
end
