local _VERSION_SCP =  SCP_VERSION
local SELFIE_WIDTH = 128



local positons = {}

positons["DrawSelfie"] = {10 , ScrH() - SELFIE_WIDTH - 10}


surface.CreateFont("scp_caviar_HUD", 
{
	size = 20,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})


surface.CreateFont("scp_caviar_HUDAMMO", 
{
	size = 80,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})
--[[---------------------------------------------------------------------------
SCP BAR
---------------------------------------------------------------------------]]
local function clr( color ) return color.r, color.g, color.b, color.a end
local PANEL = {}


function PANEL:Init()
	self:SetSize( ScrW(), ScrH() )
	self.BAR_WIDTH = 200
	self.BAR_HEIGHT = 25
	self.BAR_Yoffset = 0
	self.text = "NULL"
	self.color = Color(0,0,0,125)

	self.bg = vgui.Create( "DPanel" )

	self.barMax = self.BAR_WIDTH
	self.bg:SetSize(self.BAR_WIDTH + 10, self.BAR_HEIGHT)

	self.movingBar = false
	
end

function PANEL:Paint( w, h )
	
	if(!self.movingBar) then
		surface.SetDrawColor( clr(self.color) )
	    draw.NoTexture()
	    surface.DrawPoly( self.scpBarCoords or {} )
	 
	 
	    local x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10 + self.BAR_WIDTH/2
	    local y =  (ScrH() - SELFIE_WIDTH - 10 + self.BAR_Yoffset) + 2
	    draw.DrawText( self.text, "scp_caviar_HUD", x, y, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
	end
	

	--I wish I knew how this worked
	local x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10 + self.BAR_WIDTH/2
	local y =  (ScrH() - SELFIE_WIDTH - 10 + self.BAR_Yoffset) + 2
	draw.DrawText( self.text, "scp_caviar_HUD", x, y, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
	
	render.ClearStencil()
	render.SetStencilEnable(true)
		render.SetStencilWriteMask( 1 )
		render.SetStencilTestMask( 1 )

		

		render.SetStencilFailOperation( STENCILOPERATION_REPLACE )
	    render.SetStencilPassOperation( STENCILOPERATION_ZERO )
	    render.SetStencilZFailOperation( STENCILOPERATION_ZERO )
	    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_NEVER )
    	render.SetStencilReferenceValue( 1 )

		

	    self.bg:SetPos(positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10,y)
	    self.bg.Paint = function()
	    	draw.RoundedBox( 0, 0, 0, self.bg:GetWide(), self.bg:GetTall() , Color( 0, 0, 255, 255 ) ) -- Draw a box
		end

		self.bg:SetPaintedManually(false)
		self.bg:PaintManual()
		self.bg:SetPaintedManually(true)

	    render.SetStencilFailOperation( STENCILOPERATION_ZERO )
	    render.SetStencilPassOperation( STENCILOPERATION_REPLACE )
	    render.SetStencilZFailOperation( STENCILOPERATION_ZERO )
	    render.SetStencilCompareFunction( STENCILCOMPARISONFUNCTION_EQUAL )
	    render.SetStencilReferenceValue( 1 )

	    surface.SetDrawColor( clr(self.color) )
	    draw.NoTexture()
	    surface.DrawPoly( self.scpBarCoords or {} )

		
	render.SetStencilEnable(false)
	
	draw.DrawText( self.text, "scp_caviar_HUD", x, y, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
	
 
    


	
	
end
function PANEL:RefreshTable()
	self.scpBarCoords = {
	{ x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10, y = ((ScrH() - SELFIE_WIDTH - 10) + self.BAR_HEIGHT) + self.BAR_Yoffset },
	{ x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10, y = (ScrH() - SELFIE_WIDTH - 10) + self.BAR_Yoffset},
	{ x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10 + self.BAR_WIDTH, y = (ScrH() - SELFIE_WIDTH - 10) + self.BAR_Yoffset },
	{ x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10 + self.BAR_WIDTH + 10, y = (ScrH() - SELFIE_WIDTH - 10 + (self.BAR_HEIGHT/2)) + self.BAR_Yoffset },
	{ x = positons["DrawSelfie"][1]  + SELFIE_WIDTH + 10 + self.BAR_WIDTH + 10, y = (ScrH() - SELFIE_WIDTH - 10 + self.BAR_HEIGHT) + self.BAR_Yoffset },
	}
end
function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

vgui.Register( "scp_hud_bar", PANEL, "Panel" )


---------------------------------------------------------------------------------
if !HUD then
	HUD = {}
end

function HUD:TextSize( text, font )
 
	surface.SetFont( font );
	return surface.GetTextSize( text ) + 10;
 
end

function HUD:Init()
	
	if( HUD.selfie ) then
		HUD.selfie:Remove()
	end

	local p = positons["DrawSelfie"]

	local mdl = vgui.Create( "DModelPanel" )
	mdl:SetPos( p[1], p[2] )
	mdl:SetSize( SELFIE_WIDTH, SELFIE_WIDTH + 2 )
	mdl:SetFOV( 12 )
	mdl:SetCamPos( Vector( 200, 0, 50 ) )
	mdl:SetLookAt( Vector( 0, 0, 55 ) )
	--mdl:SetModel(LocalPlayer():GetModel())
	
	mdl:SetDirectionalLight( BOX_RIGHT, Color(150, 150, 150) )
	mdl:SetDirectionalLight( BOX_LEFT, Color( 255, 100, 100 ) )
	mdl:SetAmbientLight( Vector( -64, -64, -64 ) ) 
	mdl:SetColor( Color( 200, 200, 200 ) )
	mdl:SetAnimated( false )																																							
	mdl.Angles = Angle( 0, 0, 0 )
	mdl:SetMouseInputEnabled(false)

	function mdl:LayoutEntity( ent )
		
	end
	HUD.selfie = mdl


	if(hpBar) then hpBar:Remove() end
	hpBar = vgui.Create("scp_hud_bar")
	hpBar:RefreshTable()
	hpBar.text = "Health"
	hpBar.movingBar = true
	hpBar.color = Color(192, 57, 43, 200)
	
	if(stamBar) then stamBar:Remove() end
	stamBar = vgui.Create("scp_hud_bar")
	stamBar.BAR_Yoffset = 35
	stamBar:RefreshTable()
	stamBar.text = "Stamina"
	stamBar.movingBar = true
	stamBar.color = Color(41, 128, 185, 200)

	if(ablBar) then ablBar:Remove() end
	ablBar = vgui.Create("scp_hud_bar")
	ablBar.text = "Special Ability"
	ablBar.BAR_Yoffset = 70
	ablBar.movingBar = true
	ablBar.color = Color(39, 174, 96, 125)
	ablBar:RefreshTable()

	if(jobBar) then jobBar:Remove() end
	jobBar = vgui.Create("scp_hud_bar")
	jobBar.BAR_Yoffset = 105
	jobBar:RefreshTable()




	
	
end
HUD:Init()


function HUD:DrawSelfie()
	local p = positons["DrawSelfie"]
	surface.SetDrawColor( Color( 0, 0, 0, 125 ) )
	surface.DrawOutlinedRect( p[1], p[2], SELFIE_WIDTH, SELFIE_WIDTH + 2 )
	draw.RoundedBox( 0, p[1], p[2], SELFIE_WIDTH, SELFIE_WIDTH + 2, Color( 0, 0, 0, 125 ) ) -- Draw a box


	
	
end

 
function HUD:DrawPlayerNames()
	local ent = LocalPlayer():GetEyeTrace().Entity
	if( !IsValid(ent) and !ent:IsPlayer()) then return end
	local dist = LocalPlayer():GetPos():DistToSqr(ent:GetPos())

	if( dist < 100000) then
		local pos = ent:GetPos():ToScreen().y/2
		if(IsValid(ent) and ent:IsPlayer() and ent:Alive()) then
			surface.SetFont("scp_caviar_HUD")
			if(ent:LookupAttachment("eyes")) then
				if(ent:LookupAttachment("eyes")) then
					local ID = ent:LookupAttachment("eyes")
					local eyes = ent:GetAttachment( ID )
					if(eyes) then
						pos = (eyes.Pos + Vector(0,0,10)):ToScreen()
						pos = pos.y
					end
					
				end
			end
			
			
			draw.DrawText( ent:Nick(), "scp_caviar_HUD", ent:GetPos():ToScreen().x, pos, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
		end
	end
end

local hpAdd = 0
local smooth=0
local jobText = "No Job"
function GM:HUDPaint()
	local p = positons["DrawSelfie"]

	HUD:DrawSelfie()
	if(HUD.selfie) then 
		if(player_manager.GetPlayerClass(LocalPlayer()) == "player_scp_173") then 
			HUD.selfie:SetModel("models/vinrax/scp173/scp173.mdl") 
			function HUD.selfie:LayoutEntity( ent )
				ent:SetAngles(Angle(0,90,0))
				HUD.selfie: SetCamPos( Vector( 300, 0, 100 ) )
			end
		else
			HUD.selfie:SetModel(LocalPlayer():GetModel())
			function HUD.selfie:LayoutEntity( ent )
				HUD.selfie:SetCamPos( Vector( 200, 0, 50 ) )
			end
		end
		HUD.selfie:Refresh() 
	end
	if(jobBar) then
		local class = player_manager.GetPlayerClass(LocalPlayer())
		if (jobBar.text != nil and class != nil) then jobBar.text = "Job: " .. SCP_JOB_DISPLAY_NAMES[class] or "None" end
	end
	HUD:DrawPlayerNames()
	
	if(LocalPlayer():Health() <= 10) then
		hpAdd = 0
	else
		hpAdd = 10
	end
	if(hpBar) then
		hpBar.bg:SetSize(hpBar.barMax * LocalPlayer():Health()/LocalPlayer():GetMaxHealth() + hpAdd,hpBar.bg:GetTall() )	
	end
	
	if(LocalPlayer():GetNWInt("ply_ability") <= 10) then
		ablAdd = 0
	else
		ablAdd = 10
	end
	if(ablBar) then
		ablBar.bg:SetSize(ablBar.barMax * LocalPlayer():GetNWInt("ply_ability")/100 + ablAdd,ablBar.bg:GetTall() )	
	end

	
	--ALPHA
	local alphaTxt = _VERSION_SCP
	local x = ScrW() - HUD:TextSize(alphaTxt, "scp_caviar_HUD") / 2
	draw.DrawText( alphaTxt, "scp_caviar_HUD", x - 10, 10, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )

	
	if (IsValid( LocalPlayer():GetActiveWeapon() )) then
		if(LocalPlayer():GetActiveWeapon():Clip1() and LocalPlayer():GetActiveWeapon():Clip1() != -1 and LocalPlayer():Alive() or false) then
			red = LocalPlayer():GetActiveWeapon():Clip1()/LocalPlayer():GetActiveWeapon():GetMaxClip1() * 255
			local x = ScrW() - HUD:TextSize(alphaTxt, "scp_caviar_HUD") + HUD:TextSize(alphaTxt, "scp_caviar_HUD") * 0.25
			local y = ScrH() - HUD:TextSize(alphaTxt, "scp_caviar_HUD")/2
			draw.DrawText( LocalPlayer():GetActiveWeapon():Clip1() .. "/".. LocalPlayer():GetAmmoCount(LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType()), "scp_caviar_HUDAMMO", x, y, Color( 255, red, red, 255 ), TEXT_ALIGN_CENTER )
		end
	end
	
	
	
end

local tab = {
	["$pp_colour_addr"] = 0,
	["$pp_colour_addg"] = 0.04,
	["$pp_colour_addb"] = 0,
	["$pp_colour_brightness"] = -0.04,
	["$pp_colour_contrast"] = 1.35,
	["$pp_colour_colour"] = 5,
	["$pp_colour_mulr"] = 0,
	["$pp_colour_mulg"] = 0,
	["$pp_colour_mulb"] = 0
}

function GM:RenderScreenspaceEffects()
	if(player_manager.GetPlayerClass(LocalPlayer()) == "player_scp_049_minion") then
		DrawColorModify( tab ) --Draws Color Modify effect
	end
	
	
end


