--[[---------------------------------------------------------------------------
MOTDBACKGROUND
---------------------------------------------------------------------------]]
local PANEL = {}


function PANEL:Init()
	self:SetSize( ScrW() , ScrH())
	
end

function PANEL:Paint( w, h )
	draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 200 ) )
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "motdBackground", PANEL, "Panel" )

surface.CreateFont("scp_caviar_motd", 
{
	size = 80,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})

surface.CreateFont("scp_caviar_close_motd", 
{
	size = 50,
	weight = "500",
	antialias = "true",
	font = "Caviar Dreams"

})
local headText = "Welcome to Terminus SCP RP"
local font = "scp_caviar_motd"

local CLOSE_WIDTH = 60
concommand.Add( "scp_opem_motd", function( ply, cmd, args )
	gui.EnableScreenClicker(true)
	local motdBG = vgui.Create("motdBackground")
	motdBG:Center()
	local motdBGInner = vgui.Create("motdBackground", motdBG)
	motdBGInner:SetSize(motdBG:GetWide() * 0.95,motdBG:GetTall() * 0.95 )
	motdBGInner:Center()
	motdBGInner.Paint = function()
		local w, h = motdBGInner:GetWide(), motdBGInner:GetTall()
		
		draw.RoundedBox( 0, 0, 0, w, h, Color( 50, 50, 50 ))

		surface.SetFont( font )
		local width, height = surface.GetTextSize( headText ) 
		draw.RoundedBox( 0, 0, 0, w, height + 10, Color( 0, 0, 0, 125 ) )

		draw.DrawText(headText,font, width/2 + 10, 10,Color(255,255,255,255),TEXT_ALIGN_CENTER)
		surface.SetFont( "scp_caviar_HUD" )
		
		width, height = surface.GetTextSize( "Made by 47goon" )
		draw.DrawText("Made by 47goon","scp_caviar_HUD", w-width/2- 10, 5,Color(255,255,255,255),TEXT_ALIGN_CENTER)
	end

	local motdClose = vgui.Create("DButton", motdBGInner)
	motdClose:SetText("")
	motdClose:SetSize(motdBGInner:GetWide() * 0.25, CLOSE_WIDTH)
	motdClose:Center()
	local x,y = motdClose:GetPos()
	motdClose:SetPos(x,motdBGInner:GetTall() - (CLOSE_WIDTH + 20))
	

	motdClose.Paint = function()
		local w, h = motdClose:GetWide(), motdClose:GetTall()
		surface.SetFont( font )
		local width, height = surface.GetTextSize( "Close" )
		
		if(motdClose:IsHovered()) then
			draw.RoundedBox( 25, 0, 0, w, h, Color( 0, 0, 0, 125 ))
		else
			draw.RoundedBox( 25, 0, 0, w, h, Color( 0, 0, 0, 200 ))
		end
		draw.DrawText("Close","scp_caviar_close_motd", w/2 , 5,Color(255,255,255,255),TEXT_ALIGN_CENTER)
	end

	motdClose.DoClick = function()
		motdBG:Remove()
		gui.EnableScreenClicker(false)
	end

	local webpage = vgui.Create("motdBackground", motdBGInner )
	webpage:SetSize(motdBGInner:GetWide() * 0.95,motdBGInner:GetTall() * 0.80)
	webpage:Center()
	
	local html = vgui.Create( "HTML", webpage )
	html:Dock( FILL )
	html:OpenURL( "http://terminuscentral.com/servers/semiscprp/semiscprpmotd.html/" )

	
end)