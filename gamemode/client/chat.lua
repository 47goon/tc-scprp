
hook.Remove("HUDShouldDraw", "goon_hidgoonChat")
hook.Add("HUDShouldDraw", "goonChat_hidedefault", function( name )
  if name == "CHudChat" then
    return false
  end
end)


--CREDIT TO https://github.com/Exho1/goonChat/blob/master/lua/autorun/cl_chat.lua#L345 4 IDEAS AND CODE
goonChat = {}

goonChat.config = {
  timeStamps = true,
  position = 1, 
  fadeTime = 12,
}

surface.CreateFont("scp_caviar_chat1", 
{
  size = 20,
  weight = 500,
  antialias = "true",
  font = "Caviar Dreams"

})

surface.CreateFont("scp_caviar_chat2", 
{
  size = 20,
  weight = 10000,
  antialias = "true",
  font = "Caviar Dreams"

})

function goonChat.buildBox()
  goonChat.frame = vgui.Create("DFrame")
  goonChat.frame:SetSize( ScrW()*0.375, ScrH()*0.25 )
  goonChat.frame:SetTitle("")
  goonChat.frame:ShowCloseButton( false )
  goonChat.frame:SetDraggable( true )
  goonChat.frame:SetSizable( true )
  goonChat.frame:SetPos( ScrW()*0.0116, (ScrH() - goonChat.frame:GetTall()) - ScrH()*0.177)
  goonChat.frame:SetMinWidth( 300 )
  goonChat.frame:SetMinHeight( 100 )
  goonChat.frame.Paint = function( self, w, h )
    goonChat.blur( self, 10, 20, 255 )
    draw.RoundedBox( 0, 0, 0, w, h, Color( 50, 50, 50, 100 ) )
    
    draw.RoundedBox( 0, 0, 0, w, 25, Color( 80, 80, 80, 100 ) )
  end
  goonChat.oldPaint = goonChat.frame.Paint
  goonChat.frame.Think = function()
    if input.IsKeyDown( KEY_ESCAPE ) then
      goonChat.hideBox()
    end
  end

  local serverName = vgui.Create("DLabel", goonChat.frame)
  serverName:SetText( GetHostName() )
  serverName:SetFont( "scp_caviar_chat1" )
  serverName:SizeToContents()
  serverName:SetPos( 5, 4 )

  goonChat.entry = vgui.Create("DTextEntry", goonChat.frame) 
  goonChat.entry:SetSize( goonChat.frame:GetWide() - 20, 20 )
  goonChat.entry:SetTextColor( color_white )
  goonChat.entry:SetFont("scp_caviar_chat2")
  goonChat.entry:SetDrawBorder( false )
  goonChat.entry:SetDrawBackground( false )
  goonChat.entry:SetCursorColor( color_white )
  goonChat.entry:SetHighlightColor( Color(52, 152, 219) )
  goonChat.entry:SetPos( 10, goonChat.frame:GetTall() - goonChat.entry:GetTall() - 5 )
  goonChat.entry.Paint = function( self, w, h )
    draw.RoundedBox( 0, 0, 0, w, h, Color( 30, 30, 30, 100 ) )
    derma.SkinHook( "Paint", "TextEntry", self, w, h )
  end

  goonChat.entry.OnTextChanged = function( self )
    if self and self.GetText then 
      gamemode.Call( "ChatTextChanged", self:GetText() or "" )
    end
  end

  goonChat.entry.OnKeyCodeTyped = function( self, code )
    local types = {"", "teamchat", "console", "ooc", "advert"}

    if string.Trim( self:GetText() ) == "//" or string.Trim( self:GetText() ) == "/ooc"  then
      goonChat.ChatType = types[4]
      self:SetText("")
    end

    if string.Trim( self:GetText() ) == "/advert"  then
      goonChat.ChatType = types[5]
      self:SetText("")
    end

    if code == KEY_ESCAPE then

      goonChat.hideBox()
      gui.HideGameUI()

    elseif code == KEY_TAB then
      
      goonChat.TypeSelector = (goonChat.TypeSelector and goonChat.TypeSelector + 1) or 1
      
      if goonChat.TypeSelector > 3 then goonChat.TypeSelector = 1 end
      if goonChat.TypeSelector < 1 then goonChat.TypeSelector = 3 end
      
      goonChat.ChatType = types[goonChat.TypeSelector]

      timer.Simple(0.001, function() goonChat.entry:RequestFocus() end)

    elseif code == KEY_ENTER then
      -- Replicate the client pressing enter
      
      if string.Trim( self:GetText() ) != "" then
        if goonChat.ChatType == types[2] then
          LocalPlayer():ConCommand("say_team \"" .. (self:GetText() or "") .. "\"")
        elseif goonChat.ChatType == types[3] then
          LocalPlayer():ConCommand(self:GetText() or "")
        elseif goonChat.ChatType == types[4] then
          LocalPlayer():ConCommand("say \"" .. "//" .. self:GetText() .. "\"")
        elseif goonChat.ChatType == types[5] then
          LocalPlayer():ConCommand("say \"" .. "/advert" .. self:GetText() .. "\"")
        else
          LocalPlayer():ConCommand("say \"" .. self:GetText() .. "\"")
        end
      end

      goonChat.TypeSelector = 1
      goonChat.hideBox()
    end
  end

  goonChat.chatLog = vgui.Create("RichText", goonChat.frame) 
  goonChat.chatLog:SetSize( goonChat.frame:GetWide() - 10, goonChat.frame:GetTall() - 60 )
  goonChat.chatLog:SetPos( 5, 30 )
  goonChat.chatLog.Paint = function( self, w, h )
    draw.RoundedBox( 0, 0, 0, w, h, Color( 30, 30, 30, 100 ) )
  end
  goonChat.chatLog.Think = function( self )
    if goonChat.lastMessage then
      if CurTime() - goonChat.lastMessage > goonChat.config.fadeTime then
        self:SetVisible( false )
      else
        self:SetVisible( true )
      end
    end
    self:SetSize( goonChat.frame:GetWide() - 10, goonChat.frame:GetTall() - goonChat.entry:GetTall() - serverName:GetTall() - 20 )
    
  end
  goonChat.chatLog.PerformLayout = function( self )
    self:SetFontInternal("scp_caviar_chat2")
    self:SetFGColor( color_white )
  end
  goonChat.oldPaint2 = goonChat.chatLog.Paint
  
  local text = "Say :"

  local say = vgui.Create("DLabel", goonChat.frame)
  say:SetText("")
  surface.SetFont( "scp_caviar_chat2")
  local w, h = surface.GetTextSize( text )
  say:SetSize( w + 5, 20 )
  say:SetPos( 5, goonChat.frame:GetTall() - goonChat.entry:GetTall() - 5 )
  
  say.Paint = function( self, w, h )
    draw.RoundedBox( 0, 0, 0, w, h, Color( 30, 30, 30, 100 ) )
    draw.DrawText( text, "scp_caviar_chat1", 2, 1, color_white )
  end

  say.Think = function( self )
    local types = {"", "teamchat", "console", "ooc", "advert"}
    local s = {}

    if goonChat.ChatType == types[2] then 
      text = "Say (TEAM) :" 
    elseif goonChat.ChatType == types[3] then
      text = "Console :"
    elseif goonChat.ChatType == types[4] then
      text = "OOC :"
    elseif goonChat.ChatType == types[5] then
      text = "Advert :"
    else
      text = "Say :"
      s.pw = 45
      s.sw = goonChat.frame:GetWide() - 50
    end

    if s then
      if not s.pw then s.pw = self:GetWide() + 10 end
      if not s.sw then s.sw = goonChat.frame:GetWide() - self:GetWide() - 15 end
    end

    local w, h = surface.GetTextSize( text )
    self:SetSize( w + 5, 20 )
    self:SetPos( 5, goonChat.frame:GetTall() - goonChat.entry:GetTall() - 5 )

    goonChat.entry:SetSize( s.sw, 20 )
    goonChat.entry:SetPos( s.pw, goonChat.frame:GetTall() - goonChat.entry:GetTall() - 5 )
  end
  goonChat.hideBox()
end

--// Panel based blur function by Chessnut from NutScript
local blur = Material( "pp/blurscreen" )
function goonChat.blur( panel, layers, density, alpha )
  -- Its a scientifically proven fact that blur improves a script
  local x, y = panel:LocalToScreen(0, 0)

  surface.SetDrawColor( 255, 255, 255, alpha )
  surface.SetMaterial( blur )

  for i = 1, 3 do
    blur:SetFloat( "$blur", ( i / layers ) * density )
    blur:Recompute()

    render.UpdateScreenEffectTexture()
    surface.DrawTexturedRect( -x, -y, ScrW(), ScrH() )
  end
end

function goonChat.hideBox()
  goonChat.frame.Paint = function() end
  goonChat.entry:SetText( "" )
  gamemode.Call( "ChatTextChanged", "" )

  goonChat.ChatType = ""

  goonChat.chatLog.Paint = function() end
  
  goonChat.chatLog:SetVerticalScrollbarEnabled( false )
  goonChat.chatLog:GotoTextEnd()
  
  goonChat.lastMessage = goonChat.lastMessage or CurTime() - goonChat.config.fadeTime

  local children = goonChat.frame:GetChildren()
  for _, pnl in pairs( children ) do
    if pnl == goonChat.frame.btnMaxim or pnl == goonChat.frame.btnClose or pnl == goonChat.frame.btnMinim then continue end
    
    if pnl != goonChat.chatLog then
      pnl:SetVisible( false )
    end
  end

  -- Give the player control again
  goonChat.frame:SetMouseInputEnabled( false )
  goonChat.frame:SetKeyboardInputEnabled( false )
  gui.EnableScreenClicker( false )
end

function goonChat.showBox()
  goonChat.frame.Paint = goonChat.oldPaint

  goonChat.chatLog.Paint = goonChat.oldPaint2
  
  goonChat.chatLog:SetVerticalScrollbarEnabled( true )
  goonChat.lastMessage = nil
  goonChat.frame:MakePopup()
  goonChat.entry:RequestFocus()

  local children = goonChat.frame:GetChildren()
  for _, pnl in pairs( children ) do
    if pnl == goonChat.frame.btnMaxim or pnl == goonChat.frame.btnClose or pnl == goonChat.frame.btnMinim then continue end
    
    pnl:SetVisible( true )
  end
  
  -- Make sure other addons know we are chatting
  gamemode.Call("StartChat")
end

local oldAddText = chat.AddText

--// Overwrite chat.AddText to detour it into my chatbox
function chat.AddText(...)
  if not goonChat.chatLog then
    goonChat.buildBox()
  end
  
  local msg = {}
  -- Iterate through the strings and colors
  for _, obj in pairs( {...} ) do
    if type(obj) == "table" then
      goonChat.chatLog:InsertColorChange( obj.r, obj.g, obj.b, obj.a )
      table.insert( msg, Color(obj.r, obj.g, obj.b, obj.a) )
    elseif type(obj) == "string"  then
      goonChat.chatLog:AppendText( obj )
      table.insert( msg, obj )
    elseif obj:IsPlayer() then
      local ply = obj
      
      if goonChat.config.timeStamps then
        goonChat.chatLog:InsertColorChange( 130, 130, 130, 255 )
        goonChat.chatLog:AppendText( "["..os.date("%X").."] ")
      end
      
      if goonChat.config.seeChatTags and ply:GetNWBool("eChat_tagEnabled", false) then
        local col = ply:GetNWString("eChat_tagCol", "255 255 255")
        local tbl = string.Explode(" ", col )
        goonChat.chatLog:InsertColorChange( tbl[1], tbl[2], tbl[3], 255 )
        goonChat.chatLog:AppendText( "["..ply:GetNWString("eChat_tag", "N/A").."] ")
      end
      
      local col = GAMEMODE:GetTeamColor( obj )
      goonChat.chatLog:InsertColorChange( col.r, col.g, col.b, 255 )
      goonChat.chatLog:AppendText( obj:Nick() )
      table.insert( msg, obj:Nick() )
    end
  end
  goonChat.chatLog:AppendText("\n")
  
  goonChat.chatLog:SetVisible( true )
  goonChat.lastMessage = CurTime()
  goonChat.chatLog:InsertColorChange( 255, 255, 255, 255 )
--  oldAddText(unpack(msg))
end

--// Write any server notifications
hook.Remove( "ChatText", "echat_joinleave")
hook.Add( "ChatText", "echat_joinleave", function( index, name, text, type )
  if not goonChat.chatLog then
    goonChat.buildBox()
  end
  
  if type != "chat" then
    goonChat.chatLog:InsertColorChange( 0, 128, 255, 255 )
    goonChat.chatLog:AppendText( text.."\n" )
    goonChat.chatLog:SetVisible( true )
    goonChat.lastMessage = CurTime()
    return true
  end
end)


hook.Remove("PlayerBindPress", "goonChat_hijackbind")
hook.Add("PlayerBindPress", "goonChat_hijackbind", function(ply, bind, pressed)
  if string.sub( bind, 1, 11 ) == "messagemode" then
    if IsValid( goonChat.frame ) then
      goonChat.showBox()
    else
      goonChat.buildBox()
      goonChat.showBox()
    end
    return true
  end
end)

--// Modify the Chatbox for align.
local oldGetChatBoxPos = chat.GetChatBoxPos
function chat.GetChatBoxPos()
  return goonChat.frame:GetPos()
end

function chat.GetChatBoxSize()
  return goonChat.frame:GetSize()
end

chat.Open = goonChat.showBox
function chat.Close(...) 
  if IsValid( goonChat.frame ) then 
    goonChat.hideBox(...)
  else
    goonChat.buildBox()
    goonChat.showBox()
  end
end




function GM:ChatText(index, name, text, typ)
  if (typ == "joinleave" or typ == "namechange" or typ == "servermsg"  or typ == "teamchange" ) then return true end
end



function ChatCommands( ply, strText, bTeamOnly, bplyIsDead )
  local Text = string.lower(strText)
  if(string.sub(Text, 1, 4) == "/ooc") then 
    strText = string.sub( strText, 5, string.len( strText ) )
    chat.AddText(Color( 255, 0, 255 ), "[OOC] " .. ply:Nick(), Color( 255, 255, 255 ), ":", strText )
    return true
  end

  if(string.sub(Text, 1, 2) == "//") then 
    strText = string.sub( strText, 3, string.len( strText ) )
    chat.AddText(Color( 255, 0, 255 ), "[OOC] " .. ply:Nick(), Color( 255, 255, 255 ), ":", strText )
    return true
  end

  if(string.sub(Text, 1, 5) == "/looc") then 
    strText = string.sub( strText, 6, string.len( strText ) )
    chat.AddText(Color( 0, 255, 0 ), "[LOOC] " .. ply:Nick(), Color( 255, 255, 255 ), ":", strText )
    return true
  end

  if(string.sub(Text, 1, 7) == "/advert") then 
    strText = string.sub( strText, 8, string.len( strText ) )
    chat.AddText(Color( 255, 255, 0 ), "[Advert] " .. ply:Nick(), Color( 255, 255, 255 ), ":", strText )
    return true
  end
  
  chat.AddText(Color( 135,206,250 ), ply:Nick(), Color( 255, 255, 255 ), ": ", strText )
  return true
end
hook.Add( "OnPlayerChat", "scp_chat", ChatCommands )
