SCP_SCOREBOARD = SCP_SCP_SCOREBOARD or {}

SCP_SCOREBOARD.init = false


SCP_SCOREBOARD.titleHeight = 100
SCP_SCOREBOARD.titleWidth = 500
SCP_SCOREBOARD.titleText = "Terminus " .. SCP_VERSION

SCP_SCOREBOARD.holderHeight = 100
SCP_SCOREBOARD.holderWidth = 100

SCP_SCOREBOARD.holderHeadWidth = 10
SCP_SCOREBOARD.holderHeadHeight = 10
SCP_SCOREBOARD.holderHeadTextPad = 10
SCP_SCOREBOARD.holderHeadLabels = {"Player", "Job", "Rank", "Ping"}

SCP_SCOREBOARD.playerListWidth = 10
SCP_SCOREBOARD.playerListHeight = 10

SCP_SCOREBOARD.plyListFunc = {}
SCP_SCOREBOARD.plyListFunc[1] = Nick
SCP_SCOREBOARD.plyListFunc[2] = Nick
SCP_SCOREBOARD.plyListFunc[3] = Nick
SCP_SCOREBOARD.plyListFunc[4] = Nick
local blur = Material("pp/blurscreen")

local function DrawBlur( p, a, d )
	local x, y = p:LocalToScreen(0, 0)
	surface.SetDrawColor( 255, 255, 255 )
	surface.SetMaterial( blur )
	for i = 1, d do
		blur:SetFloat( "$blur", (i / d ) * ( a ) )
		blur:Recompute()
		render.UpdateScreenEffectTexture()
		surface.DrawTexturedRect( x * -1, y * -1, ScrW(), ScrH() )
	end
end

--[[---------------------------------------------------------------------------
SCOREBOARD BACKGROUND
---------------------------------------------------------------------------]]
local PANEL = {}


function PANEL:Init()
	self:SetSize( ScrW() * 0.65, ScrH() * 0.80 )
end

function PANEL:Paint( w, h )
	DrawBlur(self,3,6)
	draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 125 ) )
end

function PANEL:Show()
	self:Refresh()
	self:SetVisible(true)
end

function PANEL:Hide()
    self:SetVisible(false)
end
function PANEL:Close()
	self:Hide()
end

vgui.Register( "scoreboardBG", PANEL, "Panel" )


local mouse = false
--[[---------------------------------------------------------------------------
MAIN
---------------------------------------------------------------------------]]
function SCP_SCOREBOARD:Init()
	self.init = true
	local w,h = 0,0
	SCP_SCOREBOARD.BG = vgui.Create("scoreboardBG")
	SCP_SCOREBOARD.BG:Center()

	SCP_SCOREBOARD.BG.Think = function(self)
		if(input.IsMouseDown( MOUSE_RIGHT )) then
			mouse = !mouse
			gui.EnableScreenClicker(mouse)
		end
	end


	SCP_SCOREBOARD.TITLE = vgui.Create("DPanel", SCP_SCOREBOARD.BG)
	SCP_SCOREBOARD.titleWidth = SCP_SCOREBOARD.BG:GetWide() * 0.95
	SCP_SCOREBOARD.TITLE:SetPos(SCP_SCOREBOARD.BG:GetWide()/2 - SCP_SCOREBOARD.titleWidth/2,10)
	SCP_SCOREBOARD.TITLE:SetSize(SCP_SCOREBOARD.titleWidth ,SCP_SCOREBOARD.titleHeight)

	SCP_SCOREBOARD.TITLE.Paint = function(self)
		surface.SetFont("scp_caviar_info")
		w,h = surface.GetTextSize(SCP_SCOREBOARD.titleText) 
		surface.SetTextColor(255,255,255,255)
		surface.SetTextPos(self:GetWide()/2 - w/2, self:GetTall()/2 - h/2)
		surface.DrawText(SCP_SCOREBOARD.titleText)
	end


	SCP_SCOREBOARD.HOLDER = vgui.Create("DPanel", SCP_SCOREBOARD.BG)
	SCP_SCOREBOARD.holderWidth = SCP_SCOREBOARD.BG:GetWide() * 0.95
	SCP_SCOREBOARD.holderHeight = SCP_SCOREBOARD.BG:GetTall() * 0.85
	SCP_SCOREBOARD.HOLDER:SetSize(SCP_SCOREBOARD.holderWidth, SCP_SCOREBOARD.holderHeight)
	SCP_SCOREBOARD.HOLDER:SetPos(SCP_SCOREBOARD.BG:GetWide()/2 - SCP_SCOREBOARD.holderWidth/2, SCP_SCOREBOARD.BG:GetTall()/2 - SCP_SCOREBOARD.holderHeight/2 + SCP_SCOREBOARD.titleHeight/2 - 10)
	SCP_SCOREBOARD.HOLDER.Paint = function(self)
		draw.RoundedBox( 5, 0, 0, self:GetWide(), self:GetTall(), Color( 0, 0, 0, 50 ) )
	end

	SCP_SCOREBOARD.HOLDERHEAD = vgui.Create("DPanel", SCP_SCOREBOARD.HOLDER)
	SCP_SCOREBOARD.holderHeadWidth = SCP_SCOREBOARD.HOLDER:GetWide() * 0.98
	SCP_SCOREBOARD.holderHeadHeight = 50
	SCP_SCOREBOARD.HOLDERHEAD:SetSize(SCP_SCOREBOARD.holderHeadWidth, SCP_SCOREBOARD.holderHeadHeight)
	SCP_SCOREBOARD.HOLDERHEAD:SetPos(SCP_SCOREBOARD.HOLDER:GetWide()/2 - SCP_SCOREBOARD.holderHeadWidth/2, 10 )
	SCP_SCOREBOARD.HOLDERHEAD.Paint = function(self)
		draw.RoundedBox( 5, 0, 0, self:GetWide(), self:GetTall(), Color( 255, 255, 255, 10 ) )
		SCP_SCOREBOARD.holderHeadTextPad = SCP_SCOREBOARD.HOLDER:GetWide()/#SCP_SCOREBOARD.holderHeadLabels
		local cnt = 150
		for k,v in pairs(SCP_SCOREBOARD.holderHeadLabels) do
			surface.SetFont("scp_caviar_HUD")
			w,h = surface.GetTextSize(v) 
			surface.SetTextColor(255,255,255,255)
			surface.SetTextPos(cnt, self:GetTall()/2 - h/2)
			surface.DrawText(v)
			cnt = cnt + SCP_SCOREBOARD.holderHeadTextPad
		end
	end

	SCP_SCOREBOARD.PLAYERLIST = vgui.Create("DPanelList", SCP_SCOREBOARD.HOLDER )
	SCP_SCOREBOARD.PLAYERLIST.plyBars = {}
	SCP_SCOREBOARD.PLAYERLIST.nextRefresh = 0
	SCP_SCOREBOARD.playerListWidth = SCP_SCOREBOARD.HOLDER:GetWide() * 0.98
	SCP_SCOREBOARD.playerListHeight = SCP_SCOREBOARD.HOLDER:GetTall() * 0.90
	SCP_SCOREBOARD.PLAYERLIST:SetSize(SCP_SCOREBOARD.playerListWidth,SCP_SCOREBOARD.playerListHeight)
	SCP_SCOREBOARD.PLAYERLIST:SetPos(SCP_SCOREBOARD.HOLDER:GetWide()/2 - SCP_SCOREBOARD.playerListWidth/2,SCP_SCOREBOARD.HOLDER:GetTall()/2 - SCP_SCOREBOARD.playerListHeight/2 + SCP_SCOREBOARD.holderHeadHeight/2)
	SCP_SCOREBOARD.PLAYERLIST.Paint = function(self)
		--draw.RoundedBox( 5, 0, 0, self:GetWide(), self:GetTall(), Color( 255, 0, 0, 50 ) )
	end

	SCP_SCOREBOARD.PLAYERLIST:SetSpacing(10)
	SCP_SCOREBOARD.PLAYERLIST:SetPadding(10)
	SCP_SCOREBOARD.PLAYERLIST:EnableVerticalScrollbar(true)
	SCP_SCOREBOARD.PLAYERLIST:EnableHorizontal(false)
	SCP_SCOREBOARD.PLAYERLIST.Refill = function(self)
		self:Clear()

		for k,v in pairs(player.GetAll()) do
			if (!v:Nick()) then return end
			if (!v:IsPlayer()) then return end
			local ID = tostring(v:SteamID())
			self.plyBars[ID] = vgui.Create("DPanel")
			self.plyBars[ID]:SetPos(0,0)
			self.plyBars[ID]:SetSize(SCP_SCOREBOARD.PLAYERLIST:GetWide(), 28)

			local Avatar = vgui.Create( "AvatarImage", self.plyBars[ID] )
			Avatar:SetSize(28,28)
			Avatar:SetPos(0,0)
			Avatar:SetPlayer(v, 64)

			local DermaButton = vgui.Create( "DButton", Avatar ) 
			DermaButton:SetText( "" )					
			DermaButton:SetPos( 0, 0 )	
			DermaButton:SetSize( Avatar:GetWide(), Avatar:GetTall() )			
			DermaButton.Paint = function()				
				draw.RoundedBox( 0, 0, 0, self:GetWide(), self:GetTall(), Color( 0, 0, 0, 0 ) )			
			end
			DermaButton.DoClick = function()				
				gui.OpenURL( "http://steamcommunity.com/profiles/" .. (v:SteamID64() or "00000000000000000") )
				SCP_SCOREBOARD.BG:Hide()			
			end
			
			self.plyBars[ID].Paint = function(self)
				draw.RoundedBox( 5, 0, 0, self:GetWide(), self:GetTall(), Color( 0, 0, 0, 50 ) )
				SCP_SCOREBOARD.holderHeadTextPad = SCP_SCOREBOARD.HOLDER:GetWide()/#SCP_SCOREBOARD.holderHeadLabels
				local cnt = 135
				local curText = "NIL"
				for l,p in pairs(SCP_SCOREBOARD.holderHeadLabels) do
					if(l==1) then curText = string.Left( v:Nick(), 10 ) end
					if(l==2) then curText = SCP_JOB_DISPLAY_NAMES[player_manager.GetPlayerClass( v )] end
					if(l==3) then 
						if (serverguard) then
							if(serverguard.player:GetRank(v) != nil) then
								curText = (serverguard.player:GetRank(v))
							end
						else
							curText = "User"
						end
					end
					if(l==4) then curText = v:Ping() end
					surface.SetFont("scp_caviar_HUD")
					w,h = surface.GetTextSize(curText) 
					surface.SetTextColor(255,255,255,255)
					surface.SetTextPos(cnt, self:GetTall()/2 - h/2)
					surface.DrawText(curText)
					cnt = cnt + SCP_SCOREBOARD.holderHeadTextPad 
				end
			end
			self:AddItem(self.plyBars[ID])
		end
	end

	--[[SCP_SCOREBOARD.PLAYERLIST.Think = function(self)
		if (self:IsVisible()) then
			if (SCP_SCOREBOARD.PLAYERLIST.nextRefresh < CurTime()) then
				SCP_SCOREBOARD.PLAYERLIST.nextRefresh = CurTime() + 0.5
				SCP_SCOREBOARD.PLAYERLIST:Refill()
			end
		end
	end--]]


end

function SCP_SCOREBOARD:show()
	if(!self.init) then SCP_SCOREBOARD:Init() end
	SCP_SCOREBOARD.BG:Show()
	SCP_SCOREBOARD.PLAYERLIST:Refill()
	gui.EnableScreenClicker(false)
end

function SCP_SCOREBOARD:hide()
	SCP_SCOREBOARD.BG:Hide()
	gui.EnableScreenClicker(false)
end

function GM:ScoreboardShow()
	SCP_SCOREBOARD:show()
end

function GM:ScoreboardHide()
	SCP_SCOREBOARD:hide()
end


