--173 106 049 682 SCP's


--CREDIT TO REDMIST FOR THIS IDEA
--[[---------------------------------------------------------------------------
SCP_STAFF JOBS
---------------------------------------------------------------------------]]
local SCP_STAFF_ID = 1

SCPJOBS.SCP_STAFF = {}

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Mtf Officer"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 3"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/kerry/class_securety.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "mtf_officer"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].info = "Recontain SCPS \nand\n terminate rouge class-d."
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 10
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Mtf Leader"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 3"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/vinrax/player/mtf_guard_player.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "mtf_leader"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].info = "Lead the mtf units \nand\n assist GenSec."
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 1
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Mtf Medic"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 3"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/kerry/class_securety_2.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "mtf_medic"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].info = "Heal."
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 2
SCP_STAFF_ID = SCP_STAFF_ID + 1


SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Security Guard"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = "Object Class: Security Staff"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/guard_pack/guard_03.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "security"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].info = "Make sure the d-class \nstay in d-block."
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 10
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Site Admin"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = "Object Class: Security Staff"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/suits/male_01_open_tie.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "site_admin"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].info = "Make sure the d-class \nstay in d-block."
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 1
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Site Admin Guard"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = "Object Class: Security Staff"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/suits/male_05_open_waistcoat.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "site_guard"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].info = "Make sure the d-class \nstay in d-block."
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 4
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "SCP Researcher"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = "Object Class: Researcher"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/kerry/class_scientist_1.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "researcher"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 10
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Biohazard Containment Unit"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/pmc_5/pmc__04.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "bio"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 2
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Field Agent"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/pmc_4/pmc__07.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "field_agent"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 2
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Chaos Insurgent Leader"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/pmc_2/pmc__01.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "ci_leader"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 2
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Chaos Insurgent Grunt"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/pmc_2/pmc__11.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "ci_grunt"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 2
SCP_STAFF_ID = SCP_STAFF_ID + 1

SCPJOBS.SCP_STAFF[SCP_STAFF_ID] = {}
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].jobTitle = "Chaos Insurgent Infiltration"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].NONSCPurityLevel = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class = ""
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].model = "models/player/pmc_6/pmc__14.mdl"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].class_filename = "ci_inf"
SCPJOBS.SCP_STAFF[SCP_STAFF_ID].maxplayers = 2
SCP_STAFF_ID = SCP_STAFF_ID + 1

--[[---------------------------------------------------------------------------
SCP JOBS
---------------------------------------------------------------------------]]
local SCP_ID = 1

SCPJOBS.SCP = {}

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 173"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: EUCLID"
SCPJOBS.SCP[SCP_ID].model = "models/breach173.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_173"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1


SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 682"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: Keter"
SCPJOBS.SCP[SCP_ID].model = "models/scp_682/scp_682.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_682"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1


SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 106"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: Keter"
SCPJOBS.SCP[SCP_ID].model = "models/vinrax/player/scp106_player.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_106"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 096"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: Keter"
SCPJOBS.SCP[SCP_ID].model = "models/player/scp096.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_096"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 049"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: EUCLID"
SCPJOBS.SCP[SCP_ID].model = "models/vinrax/player/scp049_player.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_049"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 035"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: Keter"
SCPJOBS.SCP[SCP_ID].model = "models/vinrax/props/scp035/035_mask.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_035"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 372"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: Keter"
SCPJOBS.SCP[SCP_ID].model = "models/player/brunodoesshiz/scp372.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_372"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 999"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = "Security Level: 2"
SCPJOBS.SCP[SCP_ID].class = "Objact Class: Keter"
SCPJOBS.SCP[SCP_ID].model = "models/scp-999/scp-999.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_999"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 131-A"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = ""
SCPJOBS.SCP[SCP_ID].class = ""
SCPJOBS.SCP[SCP_ID].model = "models/scp-131-a/scp131a.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_131a"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1


SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 131-B"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = ""
SCPJOBS.SCP[SCP_ID].class = ""
SCPJOBS.SCP[SCP_ID].model = "models/scp-131-b/scp131b.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_131b"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

SCPJOBS.SCP[SCP_ID] = {}
SCPJOBS.SCP[SCP_ID].jobTitle = "SCP 079"
SCPJOBS.SCP[SCP_ID].NONSCPurityLevel = ""
SCPJOBS.SCP[SCP_ID].class = ""
SCPJOBS.SCP[SCP_ID].model = "models/props_lab/monitor02.mdl"
SCPJOBS.SCP[SCP_ID].class_filename = "scp_079"
SCPJOBS.SCP[SCP_ID].maxplayers = 1
SCP_ID = SCP_ID + 1

--[[---------------------------------------------------------------------------
NONSCP JOBS
---------------------------------------------------------------------------]]
local NONSCP_ID = 1

SCPJOBS.NONSCP = {}

SCPJOBS.NONSCP[NONSCP_ID] = {}
SCPJOBS.NONSCP[NONSCP_ID].jobTitle = "Class D"
SCPJOBS.NONSCP[NONSCP_ID].NONSCPurityLevel = "Security Level: 0"
SCPJOBS.NONSCP[NONSCP_ID].class = "Object Class:  Prison Inmate"
SCPJOBS.NONSCP[NONSCP_ID].model = "models/player/kerry/class_d_1.mdl"
SCPJOBS.NONSCP[NONSCP_ID].class_filename = "citizen"
SCPJOBS.NONSCP[NONSCP_ID].maxplayers = -1
NONSCP_ID = NONSCP_ID + 1

SCPJOBS.NONSCP[NONSCP_ID] = {}
SCPJOBS.NONSCP[NONSCP_ID].jobTitle = "Staff On Duty"
SCPJOBS.NONSCP[NONSCP_ID].NONSCPurityLevel = ""
SCPJOBS.NONSCP[NONSCP_ID].class = ""
SCPJOBS.NONSCP[NONSCP_ID].model = "models/player/kerry/class_jan_2.mdl"
SCPJOBS.NONSCP[NONSCP_ID].class_filename = "staff"
SCPJOBS.NONSCP[NONSCP_ID].maxplayers = -1
NONSCP_ID = NONSCP_ID + 1




