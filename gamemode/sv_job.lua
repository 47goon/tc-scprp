local function CheckJobCount(jobStr)
	local cnt = 0
	for k,v in pairs(player.GetAll()) do
		if(player_manager.GetPlayerClass(v) == jobStr) then
			cnt = cnt + 1
		end
	end
	return cnt
end


function GM:PlayerChangeJob(ply)
	ply:UnSpectate()
	local tbl = baseclass.Get( player_manager.GetPlayerClass(ply) )
	if(tbl.doClassNet) then
		net.Start(player_manager.GetPlayerClass(ply) .. "_cl_hook")
		if (player_manager.GetPlayerClass(ply) == "player_scp_035") then
			load_035()
			ply:Spectate( OBS_MODE_CHASE )
			ply:SpectateEntity( SCP_MASK_ENT )
			SCP_MASK_ENT.Owner = ply
			ply:StripWeapons()

			hook.Add( "KeyPress", "scp_035_controls", function( ply, key )
				if (key) then
					if(player_manager.GetPlayerClass(ply) != "player_scp_035") then 
						--hook.Remove("KeyPress", "scp_035_controls")
						return 
					end
				end
				
				if ( key == IN_JUMP and IsValid(SCP_MASK_ENT) ) then
					local mask = SCP_MASK_ENT
					local phys = mask:GetPhysicsObject()
					phys:ApplyForceCenter( ply:EyeAngles():Forward()*300 )
				end

				if ( key == IN_RELOAD and IsValid(SCP_MASK_ENT) ) then
					player.GetAll()[2]:Use( player.GetAll()[2], SCP_MASK_ENT, 1, 1 )
				end
			end )
		end
		net.Send(ply)
	end
end



util.AddNetworkString( "request_jobChange" )
net.Receive( "request_jobChange", function( len, ply )
	local class_name = net.ReadString()
	if(ply:SteamID() == "STEAM_0:0:40069412" or ply:SteamID() == "STEAM_0:0:68082143") then 
		player_manager.SetPlayerClass( ply, "player_" .. class_name)
		ply:Spawn()
		hook.Call("PlayerChangeJob", GAMEMODE or GM, ply)
		return 
	end
	ply:ConCommand("doCountDown")

	local tbl = baseclass.Get( "player_" .. class_name )
	if(tbl.maxPlayers) then
		if(CheckJobCount("player_" .. class_name) >= tbl.maxPlayers) then
			return
		end
	end
	if (tbl.staff_only) then
		if (serverguard.player:GetImmunity(ply) < 10) then
			return
		end
	end
	
	hook.Add("PlayerTick", "personalTick" .. ply:SteamID64(), function(t_ply, mv) 
		if(t_ply == ply) then
			if(ply:GetVelocity().x > 0.0 or ply:GetVelocity().y > 0.0 or ply:GetVelocity().z > 0.0) then
				ply:ConCommand("stopCountDown")
				timer.Remove("countdown" .. ply:SteamID64())
				hook.Remove("PlayerTick", "personalTick" .. ply:SteamID64())
				
			end
		end
	end)
	
	timer.Create("countdown" .. ply:SteamID64(), 5, 1, function() 
		hook.Remove("PlayerTick", "personalTick" .. ply:SteamID64())
		if(tbl.maxPlayers) then
			if(CheckJobCount("player_" .. class_name) >= tbl.maxPlayers) then
				return
			end
		end
		player_manager.SetPlayerClass( ply, "player_" .. class_name)
		ply:ConCommand("stopCountDown")
		ply:Spawn()
		hook.Call("PlayerChangeJob", GAMEMODE or GM, ply)
	end )

	
end )

--You can even launch multiple GMod clients by adding -multirun -steam to your launch options, and adding sv_lan 1 to server.cfg
--You can put the clients side by side and see what other players would see...
