if(SERVER) then
	util.AddNetworkString( "player_scp_173_cl_hook" )
	util.AddNetworkString( "SCP173_Blink" )
	util.AddNetworkString( "SCP173_ply_Blink" )
	util.AddNetworkString( "SCP173_ply_teleport" )
	local delay_173_blink = nil
	local inBlink = false

	net.Receive( "SCP173_Blink", function( len, ply )
		if(player_manager.GetPlayerClass(ply) != "player_scp_173" ) then return end
		--if( isScp( player_manager.GetPlayerClass(ply) ) ) then return end
		local tbl = net.ReadTable()
		if (delay_173_blink and CurTime() >= delay_173_blink or !delay_173_blink) then
			inBlink = true
			for k,v in pairs(tbl) do
				net.Start("SCP173_ply_Blink")
				net.Send(v)
			end
			delay_173_blink = CurTime() + math.Rand(5,15)
		end
		for k,v in pairs(tbl) do
			if(canSee(v, ply:GetPos()) and !inBlink and ply:Visible(v) and v:Alive() and !isScp(player_manager.GetPlayerClass(v)) ) then
				ply:Freeze(true)
				--ply:SetColor(Color(255,0,0))
			end

		end
		if(table.Count(tbl) <= 0  or inBlink or tbl == nil) then
			ply:Freeze(false)
			
			--ply:SetColor(Color(0,255,0))
		end
		inBlink = false
	end)

	net.Receive("SCP173_ply_teleport", function(len, ply)
		if(player_manager.GetPlayerClass(ply) != "player_scp_173" ) then return end
		local ent = net.ReadEntity()
		if(player_manager.GetPlayerClass(ent) == "player_scp_173") then return end
		if(canSee(ply, ent:GetPos()) and ply:GetPos():Distance(ent:GetPos()) <= 350 and !isScp(player_manager.GetPlayerClass(ent))) then
			if(ply:GetNWInt("ply_ability") < 100) then return end
			ply:SetPos(ent:GetPos())
			ply:SetNWInt("ply_ability", 0)
			ent:Kill()
			ply:EmitSound( "npc/barnacle/neck_snap2.wav", 90, 100, 1, CHAN_AUTO ) 
		end
	end)
end

net.Receive("player_scp_173_cl_hook", function(len, net_ply)
	local delay_173 = nil
	hook.Add("Think","SCP173_Blink_Check", function()
		if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_173" ) then 
			hook.Remove("Think","SCP173_Blink_Check") 
			return
		end
		if(delay_173 and CurTime() >= delay_173 or !delay_173) then
			local bENTS = {}
			local inS = ents.FindInSphere(LocalPlayer():GetPos(), 500)
			for k,v in pairs(inS) do
				if(v:IsPlayer() and v != LocalPlayer() and player_manager.GetPlayerClass(v) != "player_scp_173" ) then
					if(canSee(v, LocalPlayer():GetPos() )) then
						table.insert(bENTS, v)
					end
				end
			end
			net.Start("SCP173_Blink")
				net.WriteTable(bENTS)
			net.SendToServer()
			delay_173 = CurTime() + 0.1
		end
	end)
	
	hook.Add( "PreDrawHalos", "scp_173_halos", function()
		if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_173" ) then 
			hook.Remove("PreDrawHalos", "scp_173_halos") 
			return
		end
		for k,v in pairs(ents.FindInSphere( LocalPlayer():GetPos(), 500 )) do
			if(v:IsPlayer() and v == LocalPlayer():GetEyeTrace().Entity) then
				local _sin = math.sinwave( 5, 5, true );
				halo.Add( {v}, Color( 255, 0,0 ), 1 + _sin, 1 + _sin, 1 )
			end
		end
	end )

	hook.Add("KeyPress", "scp_teleeport_cl", function(ply, key)
		if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_173" ) then 
			hook.Remove("KeyPress", "scp_teleeport_cl") 
			return
		end
		local ent = LocalPlayer():GetEyeTrace().Entity
		if(key == IN_ATTACK and ent:IsPlayer() and canSee(LocalPlayer(), ent:GetPos()) and LocalPlayer():GetPos():Distance(ent:GetPos()) <= 350 ) then
			net.Start("SCP173_ply_teleport")
				net.WriteEntity(ent)
			net.SendToServer()
		end
	end)
end)

if(CLIENT) then
	net.Receive("SCP173_ply_Blink", function()
		
		local moveEye = 0
		local sqrTOP = {
			{ x = 0, y = moveEye }, --BOTTM LEFT
			{ x = 0, y = 0 }, --TOP LEFT
			{ x = ScrW(), y = 0 }, --TOP RIGHT
			{ x = ScrW(), y = moveEye } --BOTTOM RIGHT
		}

		local sqrBOTTOM = {
			{ x = 0, y = ScrH() }, --BOTTM LEFT
			{ x = 0, y = ScrH() - moveEye }, --TOP LEFT
			{ x = ScrW(), y = ScrH() - moveEye }, --TOP RIGHT
			{ x = ScrW(), y = ScrH() } --BOTTOM RIGHT
		}

		hook.Add( "HUDPaint", "player_blink", function()
			surface.SetDrawColor( 0, 0, 0, 255 )
			draw.NoTexture()
			surface.DrawPoly( sqrTOP )
			surface.DrawPoly( sqrBOTTOM )
		end )
		
		
		fraction = 0.7
		
		timer.Create( "EyeTimer1", 0, 10, function() 
			moveEye = Lerp(fraction, moveEye, ScrH()/2)
			sqrTOP = {
				{ x = 0, y = moveEye }, --BOTTM LEFT
				{ x = 0, y = 0 }, --TOP LEFT
				{ x = ScrW(), y = 0 }, --TOP RIGHT
				{ x = ScrW(), y = moveEye } --BOTTOM RIGHT
			}
			sqrBOTTOM = {
				{ x = 0, y = ScrH() }, --BOTTM LEFT
				{ x = 0, y = ScrH() - moveEye }, --TOP LEFT
				{ x = ScrW(), y = ScrH() - moveEye }, --TOP RIGHT
				{ x = ScrW(), y = ScrH() } --BOTTOM RIGHT
			}
		end )
		timer.Simple( 0.4, function()
			timer.Create( "EyeTimer2", 0, 10, function() 
				moveEye = Lerp(fraction, moveEye, 0)
				sqrTOP = {
					{ x = 0, y = moveEye }, --BOTTM LEFT
					{ x = 0, y = 0 }, --TOP LEFT
					{ x = ScrW(), y = 0 }, --TOP RIGHT
					{ x = ScrW(), y = moveEye } --BOTTOM RIGHT
				}
				sqrBOTTOM = {
					{ x = 0, y = ScrH() }, --BOTTM LEFT
					{ x = 0, y = ScrH() - moveEye }, --TOP LEFT
					{ x = ScrW(), y = ScrH() - moveEye }, --TOP RIGHT
					{ x = ScrW(), y = ScrH() } --BOTTOM RIGHT
				}
			end )
		end )
		timer.Simple( 50, function()
			hook.Remove("HUDPaint", "player_blink")
		end)
	end)
end
