if(SERVER) then
	util.AddNetworkString( "player_scp_682_cl_hook" )
end

if(CLIENT) then
	net.Receive("player_scp_682_cl_hook", function(len, net_ply)
		local function MyCalcView( ply, pos, angles, fov )
			if(player_manager.GetPlayerClass(ply) != "player_scp_682" ) then 
				hook.Remove("CalcView", "MyCalcView") 
			end
			local view = {}

			view.origin = pos-( angles:Forward()*100 )
			view.angles = angles
			view.fov = fov
			view.drawviewer = true

			return view
		end

		hook.Add( "CalcView", "MyCalcView", MyCalcView )
	end)
end

