if(SERVER) then

	hook.Add("ShouldCollide", "106_door_clip", function(ent1, ent2)
		if ( IsValid( ent1 ) and IsValid( ent2 )) then 
			if (ent1:IsPlayer() and ent2:GetClass() == "func_door" ) then
				if (player_manager.GetPlayerClass(ent1) == "player_scp_106") then
					return false 
				end
			end
		end

		return true
	end)
	util.AddNetworkString( "player_scp_106_cl_hook" )
	util.AddNetworkString( "SCP106_ply_sink" )
	util.AddNetworkString( "SCP106_ply_effects" )

	net.Receive("SCP106_ply_sink", function(len, net_ply)
		if(player_manager.GetPlayerClass(net_ply) != "player_scp_106" ) then 
			return
		end

		if(net_ply:GetNWBool("inPocket") ) then return end
		local ply = net.ReadEntity()
		if(player_manager.GetPlayerClass(ply) == "player_scp_049" or player_manager.GetPlayerClass(ply) == "player_scp_049_minion" or isScp( player_manager.GetPlayerClass(ply)) ) then return end
		if(ply.isSinking == true or ply:GetMoveType() == MOVETYPE_NONE) then return end
		
		ply:SetMoveType(MOVETYPE_NONE)
		ply:SetColor(Color(0,255,0,255))
		net.Start("SCP106_ply_effects")
		net.Send(ply)
		ply.isSinking = true

		timer.Create( ply:Nick() .. "106_sink", 0.01, ply:OBBMaxs().z + 10, function() 
			ply:SetPos(ply:GetPos() - Vector(0,0,1))
		end )

		timer.Simple( 10, function()
			ply:SetPos(Vector(-4409.372559, -6601.369629, -2209.168701))
			ply:SetColor(Color(255,255,255,255))
			ply:SetMoveType(MOVETYPE_WALK)
			timer.Remove(ply:Nick() .. "106_sink")
			ply.isSinking = false
		end)
	end)
end	

if(CLIENT) then
	net.Receive("player_scp_106_cl_hook", function(len, net_ply)
			hook.Add( "PreDrawHalos", "scp_106_halos", function()
			if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_106" ) then 
				hook.Remove("PreDrawHalos", "scp_106_halos") 
				return
			end
			for k,v in pairs(ents.FindInSphere( LocalPlayer():GetPos(), 500 )) do
				if(v:IsPlayer() and v == LocalPlayer():GetEyeTrace().Entity) then
					local _sin = math.sinwave( 5, 5, true );
					halo.Add( {v}, Color( 255, 0,0 ), 1 + _sin, 1 + _sin, 1 )
				end
			end
		end )

		hook.Add("KeyPress", "scp_106_cl_keypress", function(ply, key)
			if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_106" ) then 
				hook.Remove("KeyPress", "scp_106_cl_keypress") 
				return
			end
			local ent = LocalPlayer():GetEyeTrace().Entity
			if(key == IN_ATTACK2 && ent:IsPlayer() and canSee(LocalPlayer(), ent:GetPos()) and LocalPlayer():GetPos():Distance(ent:GetPos()) <= 350) then
				net.Start("SCP106_ply_sink")
					net.WriteEntity(ent)
				net.SendToServer()
			end
		end)
	end)
end


if(CLIENT) then
	net.Receive("SCP106_ply_effects", function()
		hook.Remove("HUDPaint", "red_box")
		hook.Remove("RenderScreenspaceEffects", "SCP106_vision")
		
		hook.Add("RenderScreenspaceEffects", "SCP106_vision", function()
			DrawMotionBlur( 0.4, 0.8, 0.1 )
			hook.Add( "HUDPaint", "red_box", function()
			    local sin = math.sinwave(4,255,true)
			    draw.RoundedBox( 0, 0, 0, ScrW(), ScrH(), Color( sin, 0, 0, 5 ) ) -- Draw a box
			end )
		end)

		timer.Simple( 30, function()
			hook.Remove("HUDPaint", "red_box")
			hook.Remove("RenderScreenspaceEffects", "SCP106_vision")
		end)
	end)
	
end