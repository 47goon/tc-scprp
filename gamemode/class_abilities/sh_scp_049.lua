if(SERVER) then
	util.AddNetworkString( "player_scp_049_cl_hook" )
	util.AddNetworkString( "SCP049_ply_infect" )

	util.PrecacheSound("scp_049/1.mp3")
	util.PrecacheSound("scp_049/2.mp3")
	util.PrecacheSound("scp_049/3.mp3")
	util.PrecacheSound("scp_049/4.mp3")

	net.Receive("SCP049_ply_infect", function(len, net_ply)
		local ply = net.ReadEntity()
		if(player_manager.GetPlayerClass(ply) == "player_scp_049" or player_manager.GetPlayerClass(ply) == "player_scp_049_minion" or isScp( player_manager.GetPlayerClass(ply)) ) then return end
		if(CheckJobCount("player_scp_049_minion") >= 3) then return end
		if (player_manager.GetPlayerClass(ply) == "player_bio") then return end
		ply.last_job = player_manager.GetPlayerClass(ply)
		player_manager.SetPlayerClass(ply, "player_scp_049_minion")
		local old_pos = ply:GetPos()
		local old_ang = ply:GetAngles()

		ply:Spawn()
		ply:SetPos(old_pos)
		ply:SetAngles(old_ang) 
		ply:SetModel("models/player/zombie_soldier.mdl")
		net_ply:EmitSound( "scp_049/"..math.random(1,4)..".mp3" )
	end)
end

if(CLIENT) then
	net.Receive("player_scp_049_cl_hook", function(len, net_ply)
			hook.Add( "PreDrawHalos", "scp_049_halos", function()
			if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_049" ) then 
				hook.Remove("PreDrawHalos", "scp_049_halos") 
				return
			end
			for k,v in pairs(ents.FindInSphere( LocalPlayer():GetPos(), 50 )) do
				if(v:IsPlayer() and v == LocalPlayer():GetEyeTrace().Entity) then
					local _sin = math.sinwave( 5, 5, true );
					halo.Add( {v}, Color( 255, 0,0 ), 1 + _sin, 1 + _sin, 1 )
				end
			end
		end )

		hook.Add("KeyPress", "scp_049_cl_keypress", function(ply, key)
			if(player_manager.GetPlayerClass(LocalPlayer()) != "player_scp_049" ) then 
				hook.Remove("KeyPress", "scp_049_cl_keypress") 
				return
			end
			local ent = LocalPlayer():GetEyeTrace().Entity
			if(key == IN_ATTACK && ent:IsPlayer() and canSee(LocalPlayer(), ent:GetPos()) and LocalPlayer():GetPos():Distance(ent:GetPos()) <= 350 ) then
				net.Start("SCP049_ply_infect")
					net.WriteEntity(ent)
				net.SendToServer()
			end
		end)
	end)
end

