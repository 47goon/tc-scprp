GM.Name = "SCP-RP"
GM.Author = "47goon"

SCP_VERSION = "SCP-RP ALPHA v0.6"

SCP_JOB_DISPLAY_NAMES = {}


for k, v in pairs(file.Find("scprp/gamemode/classes/*.lua","LUA")) do include("classes/" .. v) end
for k, v in pairs(file.Find("scprp/gamemode/class_abilities/*.lua","LUA")) do include("class_abilities/" .. v) end


for k, v in pairs(file.Find("scprp/gamemode/classes/*.lua","LUA")) do 
	v = string.Replace(v,".lua","")
	SCP_JOB_DISPLAY_NAMES[v] = baseclass.Get( v ).class_displayName or "Nil"
end
--ACECOOL
function math.sinwave( _speed, _size, _abs )
	local _sin = math.sin( RealTime( ) * ( _speed or 1 ) ) * ( _size or 1 );

	if ( _abs ) then _sin = math.abs( _sin ); end

	return _sin;
end

function canSee(ply ,entPos)
	--CREDIT FOR CODE TO CHECK ANGLES FROM: https://wiki.garrysmod.com/page/Vector/Dot
	local directionAng = math.pi / 8
	local aimvector = ply:GetAimVector()
	local entVector = entPos - ply:GetShootPos()
	local dot = aimvector:Dot( entVector ) / entVector:Length()
	if (ply:IsLineOfSightClear(entPos)) then
		return !(dot < directionAng)
	else
		return false;
	end
end

function canSee2(ply, ent)
    local view = ply:GetAimVector()
    local entPos = ent:GetShootPos()
    local enemyVector = (ply:GetShootPos() - entPos):GetNormalized()
    local dot = view:Dot(enemyVector)
    if ply:IsLineOfSightClear(entPos) then
        return dot <= (-0.3 - (0.1 * math.log10(ply:GetPos():Distance(entPos))))
    else
        return false
    end
end

function isScp(string)
	return string.find( string, "scp" )
end

function CheckJobCount(jobStr)
	local cnt = 0
	for k,v in pairs(player.GetAll()) do
		if(player_manager.GetPlayerClass(v) == jobStr) then
			cnt = cnt + 1
		end
	end
	return cnt
end

local function init()
	serverguard.plugin:Toggle("chat_settings", false)
end
hook.Add( "Initialize", "serverguard_Chat_Disable", init )



