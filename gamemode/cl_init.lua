include( "shared.lua" )

include('cl_scp_f4menu.lua')
include('cl_jobText.lua')

for k, v in pairs(file.Find("scprp/gamemode/client/*.lua","LUA")) do include("client/" .. v) end

--MODULE SYSTEM CREDI TO DARKRP SOURCE CODE
local fol = GM.FolderName .. "/gamemode/modules/"
local files, folders = file.Find(fol .. "*", "LUA")

for _, folder in pairs(folders) do
	for _, File in pairs(file.Find(fol .. folder .. "/cl_*.lua", "LUA"), true) do
        include(fol .. folder .. "/" .. File)
    end

    for _, File in pairs(file.Find(fol .. folder .. "/sh_*.lua", "LUA"), true) do
        include(fol .. folder .. "/" .. File)
    end
end

function GM:HUDShouldDraw( name )
	//CHudChat
    if name == "CHudAmmo" || name == "CHudSecondaryAmmo" || name == "CHudHealth" || name == "CHudSuit" || name == "CHudBattery" || name == "CHudCha1t" || name == "CHudCrosshair" then
        return false
    end
    return true
end

function GM:PostDrawViewModel( vm, ply, weapon )

  if ( weapon.UseHands || !weapon:IsScripted() ) then
    local hands = LocalPlayer():GetHands()
    if ( IsValid( hands ) ) then hands:DrawModel() end

  end

end




function GM:PostPlayerDraw( ply )
    if (player_manager.GetPlayerClass(ply) == "player_scp_372") then
        local scale = Vector( 0.5, 0.5, 0.5 )
        local mat = Matrix()
        mat:Scale( scale )
        mat:Translate( Vector(0,0,50) )
        ply:EnableMatrix( "RenderMultiply", mat ) 
    else
        local scale = Vector( 1, 1, 1 )
        local mat = Matrix()
        mat:Scale( scale )
        mat:Translate( Vector(0,0,0) )
        ply:EnableMatrix( "RenderMultiply", mat )
    end


    
end